# INICIANDO #

Esse é o repositório relacionado ao Projeto de Uso e Consumo que será implementado no Sistema da Açovisa

### A quem se destina esse projeto? ###

* Este projeto está relacionado ao setor de Uso e consumo da empresa, seu objeto é implementar a entrada da nota automática no ERP TOTVS.


### Etapas do projeto ###

* Instalação e configuração do NodeJS
* Implementação do projeto XML na Homologação
* Testes
* Gravação no sistema comercial.
* Insersão no ERP TOTVS (Fase atual).

### Quem está vinculado a esse projeto ###

* Lucas Mezuraro
* Roberto Santos