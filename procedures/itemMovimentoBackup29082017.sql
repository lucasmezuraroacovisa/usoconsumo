ALTER PROCEDURE  DBO.SP_ITEMMOVIMENTO 
            
                      @ID_MOVIMENTO      VARCHAR(25)
                     ,@CODCOLIGADA       INT
                     ,@IDMOV             INT
                     ,@TIPO_MOV          VARCHAR(10)
				
       
           /*### WITH ENCRYPTION ###*/

AS

/*VERSÃO DA BASE....: 10.60                                */
/*DATA CRIAÇÃO......: 24/08/2010                           */
/*RS TECNOLOGIA E INFORMAÇÕES                              */ 
/*---------------------------------------------------------*/
/*OBJETIVO..........: Manter sincronismo entre os          */ 
/* ITENS DE MOVIMENTOS dos sistemas                        */                    
/*                                                         */
/*---------------------------------------------------------*/

-- JAILSON (02/07/2012)
DECLARE
	@CFOP VARCHAR(25),
	@CFOPCTE VARCHAR(25),--ROBERTO(07/01/2016)
	@IDNATCTE VARCHAR(25),--ROBERTO(04/02/2016)
	@NATREC VARCHAR(3)

	SET @CFOP=NULL
	SET @NATREC=NULL

/****** V A L I D A Ç Ã O    D E    E X I S T E N C I A   D E   M O V   N A    A Ç O   V I S A *****/
            IF ( EXISTS (SELECT COMPENTRADA      FROM INTEGRACAO..COMP_ENTRADA_ITEM       WHERE CONVERT(VARCHAR(25),COMPENTRADA) = @ID_MOVIMENTO)
              OR EXISTS (SELECT FATURANOTAFISCAL FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM WHERE FATURANOTAFISCAL                 = @ID_MOVIMENTO)
              OR EXISTS (SELECT CTe FROM INTEGRACAO..CTE_ITEM WHERE CTe = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO)))  /*ROBERTO (22/07/2014)*/
			   ) 
                 
BEGIN


/*VERIFICAÇÃO DE ITENS DO MOVIMENTO E ELIMINAÇÃO PARA INSERIR NOVAMENTE*/
            IF EXISTS (SELECT IDMOV FROM CORPORERM..TITMMOV WHERE IDMOV =@IDMOV AND CODCOLIGADA =@CODCOLIGADA)
                       BEGIN
                            DELETE FROM CORPORERM..TTRBMOV      WHERE IDMOV =@IDMOV AND  CODCOLIGADA =@CODCOLIGADA AND NSEQITMMOV >0
                            DELETE FROM CORPORERM..TITMMOVCOMPL WHERE IDMOV =@IDMOV AND  CODCOLIGADA =@CODCOLIGADA AND NSEQITMMOV >0  
                            DELETE FROM CORPORERM..TITMMOVRATCCU WHERE IDMOV =@IDMOV AND  CODCOLIGADA =@CODCOLIGADA AND NSEQITMMOV >0 
                            DELETE FROM CORPORERM..TITMMOVHISTORICO WHERE IDMOV =@IDMOV AND  CODCOLIGADA =@CODCOLIGADA AND NSEQITMMOV >0 
                            DELETE FROM CORPORERM..TITMMOVRATDEP WHERE IDMOV =@IDMOV AND  CODCOLIGADA =@CODCOLIGADA AND NSEQITMMOV >0                             

                            DELETE FROM CORPORERM..TITMMOV WHERE IDMOV =@IDMOV AND  CODCOLIGADA =@CODCOLIGADA AND NSEQITMMOV >0 
 
                       END  

/*  I N S E R T       N  A       T I M T M O V     (# ENTRADAS #)   */
IF     EXISTS  (SELECT COMPENTRADA FROM INTEGRACAO..COMP_ENTRADA_ITEM WHERE CONVERT(VARCHAR(25),COMPENTRADA) =@ID_MOVIMENTO )                     
       AND      @TIPO_MOV = 'E'
                 
       BEGIN 
 
       INSERT INTO CORPORERMATT..titmmov (codcoligada       ,
							        	idmov             ,
								       nseqitmmov        ,
								       numerosequencial  ,
								       idprd             ,
								        quantidade        ,
													precounitario     ,
													precotabela       ,
													dataemissao       ,
													codund            ,
													quantidadeareceber,
													dataentrega       ,
													-- ROBERTO (15/08/2015)
													--datafatdec        ,
													flagefeitosaldo   ,
													quantidadeoriginal,
													codnat            ,
													idnat             ,
													flag              ,
													block             ,
													flagrefaturamento ,
													precoeditado      ,
													qtdevolumeunitario,
													codtb1flx,
													codtb1fat,
													codtb2fat,
													codccusto,
													valordesc,
													valordesp,
													-- JAILSON (30/07/2009)
													valorliquido,
													QUANTIDADETOTAL
												   )
										SELECT      
													 @CODCOLIGADA
													,@IDMOV
													,a.numeroitem
													,a.numeroitem
													,(SELECT TOP 1 IDPRD 
													  FROM CORPORERM..TPRODUTO
													  WHERE CAST(CODIGOPRD As int)=a.prodreferencia)
													,a.pesoentrada
													,a.precoentrada
													,a.precoentrada
													,B.DATANOTA
													,CASE WHEN a.unidade = 'TON' THEN 'T'
														WHEN a.unidade = 'MT' THEN 'M'
														WHEN a.unidade = 'PC' THEN 'PC'
														WHEN a.unidade IS NULL THEN 'KG'
														ELSE a.unidade
													END
													,a.pesoentrada
													,B.DATANOTA
													--ROBERTO (15/08/2015)
													--,0
													,1
													,a.pesoentrada
													,CASE WHEN a.idnat IS NULL THEN '9.999.99' ELSE 
														(SELECT b.codnat FROM CORPORERM..dnatureza b 
														WHERE b.idnat = a.idnat AND CODCOLIGADA=@CODCOLIGADA)
													END
													,CASE WHEN a.idnat IS NULL THEN '189' ELSE 
														(SELECT idnat FROM CORPORERM..dnatureza b 
														WHERE b.idnat = a.idnat AND CODCOLIGADA=@CODCOLIGADA)
													END
													,0
													,0
													,0
													,1
													,1
													,NULL

													,(SELECT b.codtb1fat FROM CORPORERM..tprd b
													 WHERE b.idprd = a.prodreferencia
													 AND CODCOLIGADA=@CODCOLIGADA) as CODTB1FAT
												
												,(SELECT para_classificacao
												   FROM CORPORERM..zdeparaclass																				--ROBERTO(31/07/2015) aedicionado coligada
												  WHERE de_natureza in (SELECT LEFT(b.codnat,5) FROM CORPORERM..dnatureza b WHERE b.idnat = a.idnat  AND CODCOLIGADA=@CODCOLIGADA))

												-- JAILSON (05/04/2011)
												,a.CODCCUSTO
												,a.arredbaixo
												,a.arredcima
												-- JAILSON (30/07/2009)
												-- Valor Liquido incluído, pois está dando erro na escrituração
												,a.valoritem
												--ROBERTO(10/09/2014)
												,a.pesoentrada
                                           
											FROM  INTEGRACAO..COMP_ENTRADA_ITEM A, INTEGRACAO..COMP_ENTRADA B
											WHERE A.COMPENTRADA = B.COMPENTRADA
											AND   CONVERT(VARCHAR(25),A.COMPENTRADA) = @ID_MOVIMENTO;
   
   
   
   
   
   /* I N S E R T      T R I B U T A Ç Ã O      D O      I T E M    D O     M O V I M E N T O   - ENTRADAS*/ 

						--> I P I
		                INSERT INTO CORPORERMATT..ttrbmov (
							codcoligada,
							idmov,
							nseqitmmov,
							codtrb,
							basedecalculo,
							aliquota,
							valor,
							fatorreducao,
							fatorsubsttrib,
							basedecalculocalculada,
							editado,
							vlrisento,
							codretencao,
							tiporecolhimento,
							codtrbbase,
							-- JAILSON (29/06/2012)
							SITTRIBUTARIA)
		                                                
		                 SELECT @codcoligada,
								@idmov,
								numeroitem,
								'IPI',
								CASE WHEN ISNULL(baseipi,0) <= 0 THEN valorisentoipi + valoroutrosipi ELSE baseipi END ,
								ipientrada,
								valoripi,
								0,
								0,
								CASE WHEN ISNULL(baseipi,0) <= 0 THEN valorisentoipi + valoroutrosipi ELSE baseipi END ,
								1,
								null,
								null,
								null,
								null,
								-- JAILSON (29/06/2012)
								IpiCst
								
						FROM  INTEGRACAO..COMP_ENTRADA_ITEM A
						WHERE A.COMPENTRADA = @ID_MOVIMENTO 

						--> ICMS
						INSERT INTO CORPORERMATT..ttrbmov (
									codcoligada,
									idmov,
									nseqitmmov,
									codtrb,
									basedecalculo,
									aliquota,
									valor,
									fatorreducao,
									fatorsubsttrib,
									basedecalculocalculada,
									editado,
									vlrisento,
									codretencao,
									tiporecolhimento,
									codtrbbase,
									-- JAILSON (29/06/2012)
									SITTRIBUTARIA)
						
						SELECT  @codcoligada,
								@idmov,
								numeroitem,
								'ICMS',

								CASE WHEN ISNULL(baseicms,0) <= 0 THEN valorisentoicms + valoroutrosicms ELSE baseicms END ,
								icmsentrada,
								valoricms, 
								basereduzidaicms,
								0,
								CASE WHEN ISNULL(baseicms,0) <= 0 THEN valorisentoicms + valoroutrosicms ELSE baseicms END ,
								1,
								null,
								null,
								null,
								null,
								-- JAILSON (29/06/2012)
								IcmsCst
								
						FROM  INTEGRACAO..COMP_ENTRADA_ITEM A
						WHERE A.COMPENTRADA = @ID_MOVIMENTO 
	
                        
                        
					/**** JAILSON (26/06/2012) - PIS E COFINS SERÁ UTILIZADO POR ITEM ****/
										
					--> C O F I N S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							'COFINS',
							ValorBaseCofins,
							AliquotaCofins,
							ValorCofins, 
							0,
							0,
							ValorBaseCofins,
							1,
							null,
							null,
							null,
							null,
							CofinsCst
						FROM  INTEGRACAO..COMP_ENTRADA_ITEM A
						WHERE A.COMPENTRADA = @ID_MOVIMENTO 


					--> P I S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							'PIS',
							ValorBasePis,
							AliquotaPis,
							ValorPis, 
							0,
							0,
							ValorBasePis,
							1,
							null,
							null,
							null,
							null,
							PisCst
						FROM  INTEGRACAO..COMP_ENTRADA_ITEM A
						WHERE A.COMPENTRADA = @ID_MOVIMENTO 


					/**** ****/
                        
                        
                        -- JAILSON (31/05/2010)
						DECLARE @CODNAT2  VARCHAR(25);
						
						
						SELECT TOP 1 @CODNAT2 = (SELECT CODNAT FROM CORPORERM..DNATUREZA D WHERE D.IDNAT = IT.IDNAT AND CODCOLIGADA=@CODCOLIGADA)
		                FROM INTEGRACAO..COMP_ENTRADA_ITEM IT
		                WHERE CONVERT(VARCHAR(25),COMPENTRADA)  = @ID_MOVIMENTO
		                ORDER BY COMPENTRADAITEM;

                        
						IF REPLACE(@codnat2,'.','') LIKE '1403%'  OR  REPLACE(@codnat2,'.','') LIKE '2403%'
						BEGIN
						
						--> Tributo de ICMS-ST
						INSERT INTO CORPORERMATT..ttrbmov (
										codcoligada,
										idmov,
										nseqitmmov,
										codtrb,
										basedecalculo,
										aliquota,
										valor,
										fatorreducao,
										fatorsubsttrib,
										basedecalculocalculada,
										editado,
										vlrisento,
										codretencao,
										tiporecolhimento,
										codtrbbase)
							
								SELECT  @codcoligada,
										@idmov,
										numeroitem,
										'ICMSST',
										baseicmsst,
										icmsentrada,
										valoricmsst,
										0,
										ivast,
										baseicmsst,
										1,
										null,
										null,
										null,
										'ICMSST'
						       FROM  INTEGRACAO..COMP_ENTRADA_ITEM A
						       WHERE A.COMPENTRADA = @ID_MOVIMENTO 
                         END				       
   
   
   
   END /*  F I M    -   I N S E R T    T I M T M O V     (# ENTRADAS #) */


/*  I N S E R T       N  A       T I M T M O V     (# SAIDAS #)   */
IF     EXISTS  (SELECT FATURANOTAFISCAL FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM WHERE CONVERT(VARCHAR(25),FATURANOTAFISCAL)=@ID_MOVIMENTO GROUP BY FATURANOTAFISCAL)                                   
       AND @TIPO_MOV = 'S'
                 
       BEGIN 
       
       
        /*ROBERTO (18/11/2015) -- VALIDA SE TEM TUBO*/
       	IF EXISTS (SELECT faturanotafiscal
					FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM 
					WHERE faturanotafiscal = @ID_MOVIMENTO
       				AND NovaDescricao LIKE '%TUBO%'
       				)
        BEGIN
        
        	UPDATE CORPORERMATT..TMOV SET CAMPOLIVRE2='TB' WHERE IDMOV=@idmov
        
        END
 
                INSERT INTO CORPORERMATT..titmmov (
                                                codcoligada       ,
                                                idmov             ,
                                                nseqitmmov        ,
                                                numerosequencial  ,
                                                idprd             ,
												valordesc         ,
												percentualdesc    ,
                                                quantidade        ,
                                                precounitario     ,
                                                precotabela       ,
                                                dataemissao       ,
                                                codund            ,
                                                quantidadeareceber,
                                                dataentrega       ,
                                                -- ROBERTO (15/08/2015)
                                                --datafatdec        ,
                                                flagefeitosaldo   ,
                                                quantidadeoriginal,
                                                codnat            ,
                                                idnat             ,
                                                flag              ,
                                                block             ,
                                                flagrefaturamento ,
                                                precoeditado      ,
                                                qtdevolumeunitario,
												codtb1flx,
												codtb1fat,
												codtb2fat,
												codccusto,
												valordesp,
												cst,
												-- JAILSON (30/07/2009)
												valorliquido,
												--- LUCAS (17/09/2013)
												QUANTIDADETOTAL
												)
                                    SELECT      @codcoligada
                                                ,@idmov
                                                ,a.numeroitem
                                                ,a.numeroitem
                                                ,(SELECT TOP 1 IDPRD 
												  FROM CORPORERM..TPRODUTO
												  WHERE CAST(CODIGOPRD As int)=a.prodreferencia AND CodColigada=@codcoligada)
												,a.valordesconto
												,CASE WHEN a.valordesconto>0 THEN (a.valordesconto / (a.peso * a.preco)) * 100
														ELSE 0
													END
                                                ,a.peso
                                                ,a.preco
                                                ,a.preco
                                                ,B.DataEmissao
												,CASE WHEN a.unidade = 'TON' THEN 'T'
														WHEN a.unidade = 'MT' THEN 'M'
														WHEN a.unidade = 'PC' THEN 'PC'
														WHEN a.unidade IS NULL THEN 'KG'
														ELSE a.unidade
													END
                                                ,a.peso
                                                ,B.DataEmissao
             -- ROBERTO (15/08/2015)
                                                --,0
             ,1
        ,a.quantidade
                                                ,ISNULL((SELECT TOP 1 b.codnat
                                                           FROM CORPORERM..dnatureza b
                                                          WHERE b.idnat = a.idnat AND CODCOLIGADA=1), '9.999.99')

												,CASE WHEN idnat IS NULL THEN '189'
													 ELSE idnat 
												END
                  ,0
                    ,0
                            ,0
                                                ,1
                                                ,1
												,NULL

												,(SELECT TOP 1 b.codtb1fat FROM CORPORERM..tprd b
												 WHERE b.idprd = a.prodreferencia
												 AND CODCOLIGADA=1) as CODTB1FAT
												
												,(SELECT TOP 1 para_classificacao
												   FROM CORPORERM..zdeparaclass
												  WHERE de_natureza in (SELECT LEFT(b.codnat,5) FROM CORPORERM..dnatureza b WHERE b.idnat = a.idnat AND CODCOLIGADA=1) )
							
												-- JAILSON (18/03/2010)
												-- ,null
												-- JAILSON (05/04/2011)
												,a.CODCCUSTO
												,a.arredcima
												,a.cst
												-- JAILSON (30/07/2009)
												-- Valor Liquido incluído, pois está dando erro na escrituração
												-- JAILSON (21/02/2011)
												, a.ValorMercadorias + a.arredcima - a.valordesconto
												,CASE WHEN a.peso IS NULL THEN 1 ELSE a.peso END

                       FROM  INTEGRACAO..FATURA_NOTA_FISCAL_ITEM A, INTEGRACAO..FATURA_NOTA_FISCAL B
				       WHERE  A.FATURANOTAFISCAL = B.FATURANOTAFISCAL
				       AND    A.FATURANOTAFISCAL = @ID_MOVIMENTO
				       
				       

   
/* I N S E R T      T R I B U T A Ç Ã O      D O      I T E M    D O     M O V I M E N T O   - SAIDAS*/ 				       


					--> I P I
					INSERT INTO CORPORERMATT..ttrbmov (CODCOLIGADA,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								-- JAILSON NFE (30/03/2009)
								modalidadebc,
								-- JAILSON (29/06/2012)
								SITTRIBUTARIA)
                                                
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							'IPI',
							CASE WHEN ISNULL(valorbaseipi,0) <= 0 THEN valorisentosipi + valoroutrosipi ELSE valorbaseipi END ,
							percentualipi,
							valoripi, 
							0,
							0,
							CASE WHEN ISNULL(valorbaseipi,0) <= 0 THEN valorisentosipi + valoroutrosipi ELSE valorbaseipi END ,
							1,
							null,
							null,
							null,
							null,
							-- JAILSON NFE (30/03/2009)
							1,
							-- JAILSON (29/06/2012)
							IpiCst
							
					FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM 
				    WHERE CONVERT(VARCHAR(25), FATURANOTAFISCAL) = @ID_MOVIMENTO


					--> I C M S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								-- JAILSON NFE (30/03/2009)
								modalidadebc,
								-- JAILSON (29/06/2012)
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							'ICMS',
							CASE WHEN ISNULL(valorbaseicms,0) <= 0 THEN valorisentosicms + valoroutrosicms ELSE valorbaseicms END ,
							percentualicms,
							valoricms, 
							-- JAILSON (07/06/2011)
							basereduzidaicms,
							0,
							CASE WHEN ISNULL(valorbaseicms,0) <= 0 THEN valorisentosicms + valoroutrosicms ELSE valorbaseicms END ,
							1,
							null,
							null,
							null,
							null,
							-- JAILSON NFE (30/03/2009)
							1,
							-- JAILSON (29/06/2012)
							SUBSTRING(CST,2,2)
					FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM 
				    WHERE CONVERT(VARCHAR(25), FATURANOTAFISCAL) = @ID_MOVIMENTO

					
					/**** JAILSON (26/06/2012) - PIS E COFINS SERÁ UTILIZADO POR ITEM ****/
										
					--> C O F I N S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							'COFINS',
							ValorBaseCofins,
							AliquotaCofins,
							ValorCofins, 
							0,
							0,
							ValorBaseCofins,
							1,
							null,
							null,
							null,
							null,
							CofinsCst
					FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM 
				    WHERE CONVERT(VARCHAR(25), FATURANOTAFISCAL) = @ID_MOVIMENTO


					--> P I S
     				INSERT INTO CORPORERM..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							'PIS',
							ValorBasePis,
							AliquotaPis,
							ValorPis, 
							0,
							0,
							ValorBasePis,
							1,
							null,
							null,
							null,
							null,
							PisCst
					FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM 
				    WHERE CONVERT(VARCHAR(25), FATURANOTAFISCAL) = @ID_MOVIMENTO


					/**** ****/
					
					-- JAILSON (02/07/2012)
					SELECT @CFOP=FATURAPADRAOFATURAMENTO
					FROM INTEGRACAO..FATURA_NOTA_FISCAL
					WHERE CONVERT(VARCHAR(25),FATURANOTAFISCAL) = @ID_MOVIMENTO
					
					IF @CFOP='6.110'
						SET @NATREC = '999'
					
					-- JAILSON NFE (30/03/2009)
					INSERT INTO CORPORERM..TITMMOVCOMPL (
								CODCOLIGADA,
								IDMOV,
								NSEQITMMOV,
								NOVADESCRICAO,
								-- JAILSON (02/07/2012)
								NATREC)
					
					SELECT  @codcoligada,
							@idmov,
							numeroitem,
							novadescricao,
							-- JAILSON (02/07/2012)
							@NATREC
					FROM INTEGRACAO..FATURA_NOTA_FISCAL_ITEM 
				    WHERE CONVERT(VARCHAR(25), FATURANOTAFISCAL) = @ID_MOVIMENTO;

					     				       
				       
				       
            END   /*   F I M   -  I N S E R T       N  A       T I M T M O V     (# SAIDAS #)   */

/*ROBERTO (22/07/2014) - INCLUSÃO DO CTE*/			
IF EXISTS (SELECT CTe FROM INTEGRACAO..CTE_ITEM WHERE CTe = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
AND (Substring(@ID_MOVIMENTO,1,3) = 'CTE') )

 BEGIN 
 
 -- ROBERTO (04/02/2016)
SELECT @IDNATCTE= IDNAT
			   FROM CORPORERM..TMOV
			   WHERE IDMOV = @IDMOV
 
SET @CFOPCTE= (SELECT TOP 1 CODNAT
			   FROM CORPORERM..DNATUREZA
			   WHERE IDNAT = @IDNATCTE)
  
SET @CFOPCTE=@CFOPCTE+'.01'
 
INSERT INTO CORPORERMATT..titmmov (
                                                codcoligada       ,
                                                idmov             ,
                                                nseqitmmov        ,
                                                numerosequencial  ,
                                                idprd             ,
												valordesc         ,
												percentualdesc    ,
                                                quantidade        ,
                                                precounitario     ,
 precotabela  ,
                   dataemissao       ,
                                                codund            ,
                                                quantidadeareceber,
                            dataentrega       ,
                      -- ROBERTO (15/08/2015)
                                                --datafatdec        ,
                                                flagefeitosaldo   ,
                                                quantidadeoriginal,
  codnat         ,
                        idnat  ,
                                                flag      ,
                                                block             ,
                                                flagrefaturamento ,
                                                precoeditado      ,
                                                qtdevolumeunitario,
												codtb1flx,
												codtb1fat,
												codtb2fat,
												codccusto,
												valordesp,
												cst,
												valorliquido,
												QUANTIDADETOTAL,
												VALORBRUTOITEM,
												VALORBRUTOITEMORIG,
												RATEIOCCUSTODEPTO)
												
                                    SELECT      @CODCOLIGADA
                                               ,@IDMOV
                                               ,1
                                               ,1
                                               --- LUCAS (16/06/2015)
                                               ,CASE WHEN @CODCOLIGADA=3 THEN 20750 ELSE 23224 END
											   ,0
											   ,0
                                               ,(SELECT SUM(PesoNF) FROM INTEGRACAO..CTE_ITEM  WHERE CTe = A.ID_CTE)
                                               ,CONVERT(NUMERIC(10,4),(A.BASECALCULO/(SELECT SUM(PesoNF) FROM INTEGRACAO..CTE_ITEM  WHERE CTe = A.ID_CTE)))
                                               ,0
                                               ,A.Data
                                               ,'UN'
											   ,(SELECT SUM(PesoNF) FROM INTEGRACAO..CTE_ITEM  WHERE CTe = A.ID_CTE)
											   ,NULL
											   --ROBERTO(15-08-2015)
                                               --,NULL
                                               ,NULL
                                               ,(SELECT SUM(PesoNF) FROM INTEGRACAO..CTE_ITEM  WHERE CTe = A.ID_CTE)
											   ,--NULL
											   (SELECT TOP 1 CODNAT
						                        FROM CORPORERM..DNATUREZA
						                        WHERE CODNAT = @CFOPCTE
                                			    AND CODCOLIGADA = @CODCOLIGADA) 
                                               ,(SELECT TOP 1 IDNAT
						                        FROM CORPORERM..DNATUREZA
						                        WHERE CODNAT = @CFOPCTE
                                			    AND CODCOLIGADA = @CODCOLIGADA) 
                                               ,0
                                               ,0
                                               ,NULL
                                               ,0
                                               ,1
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,'1.01.14.01.0001'
                                               ,NULL
                                               ,NULL
                                               ,A.BASECALCULO
											   ,(SELECT SUM(PesoNF) FROM INTEGRACAO..CTE_ITEM  WHERE CTe = A.ID_CTE)
											   ,A.BASECALCULO
											   ,A.BASECALCULO
											   ,A.BASECALCULO

                       FROM  INTEGRACAO..CTE A
				       WHERE  A.ID_CTE = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))

   
/* I N S E R T      T R I B U T A Ç Ã O      D O      I T E M   D O     M O V I M E N T O   - CTE - AÇOVISA*/ 				  
					--> I C M S
   			  	INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								modalidadebc,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							1,
							'ICMS',
							A.BASECALCULO,--  (ROBERTO 06/01/2016)
							0, --3.07 (ROBERTO 06/01/2016)
							0, --(A.BASECALCULO*0.0307) (ROBERTO 06/01/2016)
							0,
							0,
							A.BASECALCULO, -- (A.BASECALCULO*0.0307) (ROBERTO 06/01/2016)
							1,
							null,
							null,
							null,
							null,
							null,
							null
					FROM  INTEGRACAO..CTE A
	   		        WHERE  A.ID_CTE = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
									
					--> C O F I N S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								modalidadebc,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							1,
							'COFINS',
							A.BASECALCULO,
							7.6,
							(A.BASECALCULO*0.076), 
							0,
							0,
							(A.BASECALCULO*0.076),
							1,
							null,
							null,
							null,
							null,
							null,
							50
					FROM  INTEGRACAO..CTE A
	   		        WHERE  A.ID_CTE = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
									

					--> P I S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								modalidadebc,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							1,
							'PIS',
							A.BASECALCULO,
							1.65,
							(A.BASECALCULO*0.0165), 
							0,
							0,
							(A.BASECALCULO*0.0165),
							1,
							null,
							null,
							null,
							null,
							null,
							50
					FROM  INTEGRACAO..CTE A
	   		        WHERE  A.ID_CTE = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
 
 END
 
 

END 
/****** F I M -> VALIDAÇÃO  DE  EXISTENCIA  DE  ITEM  DE  MOV  NA   AÇO  VISA *****/




/*	LUCAS MEZURARO (11/05/2017) - INCLUSÃO DA NOTA DE USO CONSUMO */			
IF EXISTS (SELECT pedidoCompra FROM INTEGRACAOTE..CONSUMOPEDIDOITENS WHERE pedidoCompra = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
AND (Substring(@ID_MOVIMENTO,1,3) = 'USOCONS') )

 BEGIN 
 
 -- ROBERTO (04/02/2016)
SELECT @IDNATCTE= IDNAT
			   FROM CORPORERM..TMOV
			   WHERE IDMOV = @IDMOV
 
SET @CFOPCTE= (SELECT TOP 1 CODNAT
			   FROM CORPORERM..DNATUREZA
			   WHERE IDNAT = @IDNATCTE)
  
SET @CFOPCTE=@CFOPCTE+'.01'
 
INSERT INTO CORPORERMATT..titmmov (
                                                codcoligada       ,
                                                idmov             ,
                                                nseqitmmov        ,
                                                numerosequencial  ,
                                                idprd             ,
												valordesc         ,
												percentualdesc    ,
                                                quantidade        ,
                                                precounitario     ,
 												precotabela ,
                   								dataemissao       ,
                            codund            ,
                                                quantidadeareceber,
                            					dataentrega       ,
                      							-- ROBERTO (15/08/2015)
                                                --datafatdec      ,
                                                flagefeitosaldo   ,
                                                quantidadeoriginal,
  												codnat         	  ,
                        						idnat			  ,
                                                flag		      ,
                                                block             ,
                                                flagrefaturamento ,
                                                precoeditado      ,
                                                qtdevolumeunitario,
												codtb1flx		  ,
												codtb1fat		  ,
												codtb2fat		  ,
												codccusto		  ,
												valordesp		  ,
												cst				  ,
												valorliquido	  ,
												QUANTIDADETOTAL   ,
												VALORBRUTOITEM,
												VALORBRUTOITEMORIG,
												RATEIOCCUSTODEPTO)
												
                                    SELECT      @CODCOLIGADA
                                               ,@IDMOV
                                               ,1
                                               ,1
                                               --- LUCAS (16/06/2015)
                                               ,CASE WHEN @CODCOLIGADA=3 THEN 20750 ELSE 23224 END
											   ,0
											   ,0
                                               ,0
                                               ,0
                                               ,0
                                               ,A.DataNota
                                               ,'UN'
											   ,0
											   ,NULL
											   --ROBERTO(15-08-2015)
                                               --,NULL
                                               ,NULL
                                               ,0
											   ,--NULL
											   (SELECT TOP 1 CODNAT
						                        FROM CORPORERM..DNATUREZA
						                        WHERE CODNAT = @CFOPCTE
                                			    AND CODCOLIGADA = @CODCOLIGADA) 
                                               ,(SELECT TOP 1 IDNAT
						                        FROM CORPORERM..DNATUREZA
						                        WHERE CODNAT = @CFOPCTE
                                			    AND CODCOLIGADA = @CODCOLIGADA) 
                                               ,0
                                               ,0
                                               ,NULL
                                               ,0
                                               ,1
                                               ,NULL
                                               ,NULL
                                               ,NULL
                                               ,'1.01.14.01.0001'
                                               ,NULL
                                               ,NULL
                                               ,A.ValorTotal
											   ,0
											   ,A.ValorTotal
											   ,A.ValorTotal
											   ,A.ValorTotal

                       FROM  INTEGRACAOTE..CONSUMONOTAFISCAL A
				       WHERE  A.ID = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))

   
/* I N S E R T      T R I B U T A Ç Ã O      D O      I T E M   D O     M O V I M E N T O   - CTE - AÇOVISA*/ 				  
					--> I C M S
   			  	INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								modalidadebc,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							1,
							'ICMS',
							A.ValorTotal,--  (ROBERTO 06/01/2016)
							0, --3.07 (ROBERTO 06/01/2016)
							0, --(A.BASECALCULO*0.0307) (ROBERTO 06/01/2016)
							0,
							0,
							A.ValorTotal, -- (A.BASECALCULO*0.0307) (ROBERTO 06/01/2016)
							1,
							null,
							null,
							null,
							null,
							null,
							null
					FROM  INTEGRACAOTE..CONSUMONOTAFISCAL A
	   		        WHERE  A.ID = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
									
					--> C O F I N S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								modalidadebc,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							1,
							'COFINS',
							A.ValorTotal,
							7.6,
							(A.ValorTotal*0.076), 
							0,
							0,
							(A.ValorTotal*0.076),
							1,
							null,
							null,
							null,
							null,
							null,
							50
					FROM  INTEGRACAOTE..CONSUMONOTAFISCAL A
	   		        WHERE  A.ID = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
									

					--> P I S
     				INSERT INTO CORPORERMATT..ttrbmov (
								codcoligada,
								idmov,
								nseqitmmov,
								codtrb,
								basedecalculo,
								aliquota,
								valor,
								fatorreducao,
								fatorsubsttrib,
								basedecalculocalculada,
								editado,
								vlrisento,
								codretencao,
								tiporecolhimento,
								codtrbbase,
								modalidadebc,
								SITTRIBUTARIA)
					
					SELECT  @codcoligada,
							@idmov,
							1,
							'PIS',
							A.ValorTotal,
							1.65,
							(A.ValorTotal*0.0165), 
							0,
							0,
							(A.ValorTotal*0.0165),
							1,
							null,
							null,
							null,
							null,
							null,
							50
					FROM  INTEGRACAOTE..CONSUMONOTAFISCAL A
	   		        WHERE  A.ID = SUBSTRING(@ID_MOVIMENTO,6,LEN(@ID_MOVIMENTO))
 
 END
 
 


/****** F I M -> VALIDAÇÃO  DE  EXISTENCIA  DE  ITEM  DE  MOV  NA   AÇOVISA *****/
GO

