ALTER PROCEDURE  DBO.SP_MOVIMENTO 
            
			 @ID_MOVIMENTO								  VARCHAR(25)
			,@TIPO_MOV									  VARCHAR(10)
			/*PARAMETROS DE ENTRADA - CAMPOS TMOV v10.60*/
			,@CODCOLIGADA                                 SMALLINT                                                    
			,@CODFILIAL                                   smallint                                 
			,@CODLOC                                      varchar(15)                                 
			,@CODLOCENTREGA                               varchar(15)                                 
			,@CODLOCDESTINO                               varchar(15)                                 
			,@CODCFO                                      varchar(25)                                 
			,@CODCFONATUREZA                              varchar(25)                                 
			,@NUMEROMOV                                   varchar(35)                                 
			,@SERIE                                       varchar(8)                                 
			,@CODTMV                                      varchar(10)                                 
			,@TIPO                                        varchar(1)                                 
			,@STATUS                                      varchar(1)                                 
			,@MOVIMPRESSO                                 smallint                                 
			,@DOCIMPRESSO                                 smallint                                 
			,@FATIMPRESSA                                 smallint                                 
			,@DATAEMISSAO                                 datetime                                 
			,@DATASAIDA                                   datetime                                 
			,@DATAEXTRA1                                  datetime                                 
			,@DATAEXTRA2                                  datetime                                 
			,@CODRPR                                      varchar(15)                                 
			,@COMISSAOREPRES                              NUMERIC(15,2)                                 
			,@NORDEM                                      varchar(20)                                 
			,@CODCPG                                      varchar(5)                                 
			,@NUMEROTRIBUTOS                              smallint                                 
			,@VALORBRUTO                                  NUMERIC(15,2)                                 
			,@VALORLIQUIDO                                NUMERIC(15,2)                                 
			,@VALOROUTROS                                 NUMERIC(15,2)                                 
			,@OBSERVACAO                                  varchar(60)                                 
			,@PERCENTUALFRETE                             NUMERIC(15,2)                                 
			,@VALORFRETE                                  NUMERIC(15,2)                                 
			,@PERCENTUALSEGURO                            NUMERIC(15,2)                                 
			,@VALORSEGURO                                 NUMERIC(15,2)                                 
			,@PERCENTUALDESC                              NUMERIC(15,2)                                 
			,@VALORDESC                                   NUMERIC(15,2)                                 
			,@PERCENTUALDESP                              NUMERIC(15,2)                                 
			,@VALORDESP                                   NUMERIC(15,2)                                 
			,@PERCENTUALEXTRA1                            NUMERIC(15,2)                                 
			,@VALOREXTRA1                                 NUMERIC(15,2)                                 
			,@PERCENTUALEXTRA2                            NUMERIC(15,2)                                 
			,@VALOREXTRA2                                 NUMERIC(15,2)                                 
			,@PERCCOMISSAO                                NUMERIC(15,2)                                 
			,@CODMEN                                      varchar(5)                                 
			,@CODMEN2                                     varchar(5)                                 
			,@VIADETRANSPORTE                             varchar(15)                                 
			,@PLACA                                       varchar(10)                                 
			,@CODETDPLACA                                 varchar(2)                                 
			,@PESOLIQUIDO                                 NUMERIC(15,2)                                 
			,@PESOBRUTO                                   NUMERIC(15,2)                                 
			,@MARCA                                       varchar(10)                                 
			,@NUMERO                                      int                                 
			,@QUANTIDADE                                  int                                 
			,@ESPECIE                                     varchar(15)                                 
			,@CODTB1FAT                                   varchar(10)                                 
			,@CODTB2FAT                                   varchar(10)                                 
			,@CODTB3FAT                                   varchar(10)                                 
			,@CODTB4FAT                                   varchar(10)                                 
			,@CODTB5FAT                                   varchar(10)                                 
			,@CODTB1FLX                                   varchar(25)                                 
			,@CODTB2FLX                                   varchar(25)                                 
			,@CODTB3FLX                                   varchar(25)                                 
			,@CODTB4FLX                                   varchar(25)                                 
			,@CODTB5FLX                                   varchar(25)                                 
			,@IDMOVRELAC                                  int                                 
			,@IDMOVLCTFLUXUS                              int                                 
			,@IDMOVPEDDESDOBRADO                          int                                 
			,@CODMOEVALORLIQUIDO                          varchar(10)                                 
			,@DATABASEMOV                                 datetime                                 
			,@DATAMOVIMENTO                               datetime                                 
			,@NUMEROLCTGERADO                             smallint                                 
			,@GEROUFATURA                                 smallint                                 
			,@NUMEROLCTABERTO                             smallint                                 
			,@FLAGEXPORTACAO                              smallint                                 
			,@EMITEBOLETA                                 varchar(1)                                 
			,@CODMENDESCONTO                              varchar(5)                                 
			,@CODMENDESPESA                               varchar(5)                                 
			,@CODMENFRETE                                 varchar(5)                                 
			,@FRETECIFOUFOB                               smallint                                 
			,@USADESPFINANC                               smallint                                 
			,@FLAGEXPORFISC                               smallint                                 
			,@FLAGEXPORFAZENDA                            smallint                                 
			,@VALORADIANTAMENTO                           NUMERIC(15,2)                                 
			,@CODTRA                                      varchar(5)                                 
			,@CODTRA2                                     varchar(5)         
			,@STATUSLIBERACAO                             smallint                                 
			,@CODCFOAUX                                   varchar(25)                                 
			,@IDLOT                                       int                                 
			,@ITENSAGRUPADOS                              smallint                                 
			,@FLAGIMPRESSAOFAT                            varchar(1)                                 
			,@DATACANCELAMENTOMOV                         datetime                                 
			,@VALORRECEBIDO                               NUMERIC(15,2)                                 
			,@SEGUNDONUMERO                               varchar(20)                                 
			,@CODCCUSTO                                   varchar(25)                                 
			,@CODCXA                                      varchar(10)                                 
			,@CODVEN1                                     varchar(16)                                 
			,@CODVEN2                                     varchar(16)                                 
			,@CODVEN3                                     varchar(16)                                 
			,@CODVEN4                                     varchar(16)                                 
			,@PERCCOMISSAOVEN2                            NUMERIC(15,2)                                 
			,@CODCOLCFO                                   smallint                                 
			,@CODCOLCFONATUREZA                           smallint                                 
			,@CODUSUARIO                                  varchar(20)                                 
			,@CODFILIALENTREGA                            smallint                                 
			,@CODFILIALDESTINO                            smallint                                 
			,@FLAGAGRUPADOFLUXUS                          smallint                                 
			,@CODCOLCXA                                   SMALLINT                                 
			,@GERADOPORLOTE                               smallint                                 
			,@CODDEPARTAMENTO                             varchar(25)                                 
			,@CODCCUSTODESTINO                            varchar(25)                                 
			,@CODEVENTO                                   smallint                                 
			,@STATUSEXPORTCONT                            smallint                                 
			,@CODLOTE                                     int                                 
			,@STATUSCHEQUE                                smallint                                 
			,@DATAENTREGA                                 datetime                                 
			,@DATAPROGRAMACAO                             datetime                                 
			,@IDNAT                                       int                                 
			,@IDNAT2                                      int                                 
			,@CAMPOLIVRE1                                 varchar(100)                                 
			,@CAMPOLIVRE2                                 varchar(100)                                 
			,@CAMPOLIVRE3                                 varchar(100)                                 
			,@GEROUCONTATRABALHO                          SMALLINT                                 
			,@GERADOPORCONTATRABALHO                      SMALLINT                                 
			,@HORULTIMAALTERACAO                          datetime                                 
			,@CODLAF                                      varchar(15)                                 
			,@DATAFECHAMENTO                              datetime                                 
			,@NSEQDATAFECHAMENTO                          smallint                                 
			,@NUMERORECIBO                                varchar(12)        
			,@IDLOTEPROCESSO                              int                                 
			,@IDOBJOF                                     varchar(20)                                 
			,@CODAGENDAMENTO                              int                                 
			,@CHAPARESP                                   VARCHAR(16)                                 
			,@IDLOTEPROCESSOREFAT                         int                                 
			,@INDUSOOBJ                                   NUMERIC(15,2)                                 
			,@SUBSERIE                                    varchar(8)                                 
			,@STSCOMPRAS                                  TINYINT                                 
			,@CODLOCEXP                                   VARCHAR(15)                                 
			,@IDCLASSMOV                                  INT                                 
			,@CODENTREGA                                  VARCHAR(15)                                 
			,@CODFAIXAENTREGA                             VARCHAR(15)                                 
			,@DTHENTREGA                                  DATETIME                                 
			,@CONTABILIZADOPORTOTAL                       SMALLINT                                 
			,@CODLAFE                                     varchar(15)                                 
			,@IDPRJ                                       int                                 
			,@NUMEROCUPOM                                 int                                 
			,@NUMEROCAIXA                                 int                                 
			,@FLAGEFEITOSALDO                             smallint                                 
			,@INTEGRADOBONUM                              SMALLINT                                 
			,@CODMOELANCAMENTO                            varchar(10)                                 
			,@NAONUMERADO                                 varchar(1)                                 
			,@FLAGPROCESSADO                              SMALLINT                                 
			,@ABATIMENTOICMS                              NUMERIC(15,2)                                 
			,@TIPOCONSUMO                                 smallint                                 
			,@HORARIOEMISSAO                              datetime                                 
			,@DATARETORNO                                 datetime                                 
			,@USUARIOCRIACAO                              varchar(20)                                 
			,@DATACRIACAO                                 datetime                                 
			,@IDCONTATOENTREGA                            int                                 
			,@IDCONTATOCOBRANCA                           int                                 
			,@STATUSSEPARACAO                             varchar(1)                                 
			,@STSEMAIL                                    SMALLINT                                 
			,@VALORFRETECTRC                              NUMERIC(15,2)                                 
			,@PONTOVENDA                                  varchar(10)                                 
			,@PRAZOENTREGA                                int                                 
			,@VALORBRUTOINTERNO                           NUMERIC(15,2)                                 
			,@IDAIDF                                      smallint                                 
			,@IDSALDOESTOQUE                              int                                 
			,@VINCULADOESTOQUEFL                          SMALLINT                                 
			,@IDREDUCAOZ                                  int                                 
			,@HORASAIDA                                   datetime                                 
			,@CODMUNSERVICO                               varchar(20)                                 
			,@CODETDMUNSERV          varchar(2)                                 
			,@APROPRIADO                                  smallint                                 
			,@CODIGOSERVICO                               varchar(15)                                 
			,@DATADEDUCAO                                 datetime                                 
			,@CODDIARIO                                   varchar(5)                                 
			,@SEQDIARIO                                   varchar(9)                                 
			,@SEQDIARIOESTORNO                            varchar(9)                                 
			,@INSSEMOUTRAEMPRESA                          NUMERIC(15,2)                                 
			,@IDMOVCTRC                                   int                                 
			,@DATAPROGRAMACAOANT                          datetime                                 
			,@CODTDO                                      varchar(10)                                 
			,@VALORDESCCONDICIONAL                        NUMERIC(15,2)                                 
			,@VALORDESPCONDICIONAL                        NUMERIC(15,2)                                 
			,@CODIGOIRRF                                  varchar(10)                                 
			,@DEDUCAOIRRF                                 NUMERIC(15,2)                                 
			,@PERCENTBASEINSS                             NUMERIC(15,2)                                 
			,@PERCBASEINSSEMPREGADO                       NUMERIC(15,2)                                 
			,@CONTORCAMENTOANTIGO                         SMALLINT                                 
			,@CODDEPTODESTINO                             varchar(25)                                 
			,@DATACONTABILIZACAO                          datetime                                 
			,@CODVIATRANSPORTE                            varchar(1)                                 
			,@VALORSERVICO                                NUMERIC(15,2)                                 
			,@SEQUENCIALESTOQUE                           int                                 
			,@DISTANCIA                                   int                                 
			,@UNCALCULO                                   varchar(1)                                 
			,@FORMACALCULO                                varchar(1)                                 
			,@INTEGRADOAUTOMACAO                          smallint                                 
			,@INTEGRAAPLICACAO                            char(1)                                 
			,@CLASSECONSUMO                               varchar(1)                                 
			,@TIPOASSINANTE                               varchar(1)                                 
			,@FASE                                        varchar(1)                                 
			,@TIPOUTILIZACAO                              varchar(1)                                 
			,@GRUPOTENSAO                                 varchar(1)                                 
			,@DATALANCAMENTO                              datetime                                 
			,@EXTENPORANEO                                SMALLINT                                 
			,@RECIBONFESTATUS                             varchar(1)                                 
			,@RECIBONFETIPO                               smallint                                 
			,@RECIBONFENUMERO                             varchar(12)                                 
			,@RECIBONFESITUACAO                           smallint                                 
			,@IDMOVCFO                                    int                                 
			,@OCAUTONOMO                                  smallint                                 
			,@VALORMERCADORIAS                            NUMERIC(15,2)                                 
			,@NATUREZAVOLUMES                             varchar(30)                                 
			,@VOLUMES                             varchar(30)                                 
			,@CRO                                         smallint                                 
			,@USARATEIOVALORFIN                           SMALLINT                                 
			,@RECIBONFESERIE                              varchar(5)                                 
			,@CODCOLCFOORIGEM                             SMALLINT                                 
			,@CODCFOORIGEM                                varchar(25)                                 
			,@VALORCTRCARATEAR                            NUMERIC(15,2)                                 
			,@CODCOLCFOAUX                                smallint                                 
			,@VRBASEINSSOUTRAEMPRESA                      NUMERIC(15,2)                                 
			,@IDCEICFO                                    int                                 
			,@CHAVEACESSONFE                              varchar(44)                                 
			,@VLRSECCAT                                   NUMERIC(15,2)                                 
			,@VLRDESPACHO                                 NUMERIC(15,2)                                 
			,@VLRPEDAGIO                                  NUMERIC(15,2)                                 
			,@VLRFRETEOUTROS                              NUMERIC(15,2)                                 
			,@ABATIMENTONAOTRIB                           NUMERIC(15,2)                                 
			,@RATEIOCCUSTODEPTO                           NUMERIC(15,2)                                 
			,@VALORRATEIOLAN                              NUMERIC(15,2)                                 
			,@CODCOLCFOTRANSFAT                           SMALLINT                                 
			,@CODCFOTRANSFAT                              varchar(25)                                 
			,@CODUSUARIOAPROVADESC                        varchar(20)   




AS

/*VERSÃO DA BASE....: 10.60                                */
/*DATA CRIAÇÃO......: 13/08/2010                           */
/*RS TECNOLOGIA E INFORMAÇÕES                              */ 
/*---------------------------------------------------------*/
/*OBJETIVO..........: Manter sincronismo entre os          */ 
/*    MOVIMENTOS dos sistemas                              */                    
/*                                                         */
/*---------------------------------------------------------*/
/*ENTRADA...........:                                      */
/*SAIDA.............:                                      */

/******<  V A L I D A Ç Ã O     D E    E X I S T E N C I A    D E     M O V     N A    A Ç O V I S A    >*****/
IF ( EXISTS (SELECT COMPENTRADA FROM INTEGRACAOTE..COMP_ENTRADA
			 WHERE CONVERT(VARCHAR(25),COMPENTRADA) = @ID_MOVIMENTO)
	OR EXISTS (SELECT FATURANOTAFISCAL FROM INTEGRACAOTE..FATURA_NOTA_FISCAL
			   WHERE FATURANOTAFISCAL = @ID_MOVIMENTO)
	OR EXISTS (SELECT ID_CTE FROM INTEGRACAOTE..CTE 
	           WHERE ID_CTE = SUBSTRING(@CAMPOLIVRE1,6,LEN(@CAMPOLIVRE1)))   /*ROBERTO (22/07/2014)*/
	)

BEGIN

/*VERIFICAÇÃO DE TRIBUTACAO DO MOVIMENTO*/
IF EXISTS (SELECT IDMOV FROM CORPORERMATT..TTRBMOV
		   WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						  WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						  AND CODCOLIGADA = @CODCOLIGADA) 
           AND CODCOLIGADA = @CODCOLIGADA)
BEGIN
	DELETE FROM CORPORERMATT..TTRBMOV
	WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
				   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
				   AND CODCOLIGADA = @CODCOLIGADA)
	AND CODCOLIGADA = @CODCOLIGADA
END   

/*VERIFICAÇÃO CAMPO COMPLEMENTAR DO MOVIMENTO*/
IF EXISTS (SELECT IDMOV FROM CORPORERMATT..TMOVCOMPL
		   WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						  WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						  AND CODCOLIGADA = @CODCOLIGADA)
           AND CODCOLIGADA = @CODCOLIGADA)
BEGIN
	DELETE FROM CORPORERMATT..TMOVCOMPL
	WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
				   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
				   AND CODCOLIGADA = @CODCOLIGADA)
	AND CODCOLIGADA = @CODCOLIGADA
END

/*VERIFICAÇÃO CAMPO HISTORICO DO MOVIMENTO*/
IF EXISTS (SELECT IDMOV FROM CORPORERMATT..TMOVHISTORICO
		   WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						  WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						  AND CODCOLIGADA = @CODCOLIGADA) 
		   AND CODCOLIGADA = @CODCOLIGADA)
BEGIN
	DELETE FROM CORPORERMATT..TMOVHISTORICO
	WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
				   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
				   AND CODCOLIGADA = @CODCOLIGADA)
	AND CODCOLIGADA = @CODCOLIGADA
END 

/*VERIFICAÇÃO RATEIO DE CENTRO DE CUSTO DO MOVIMENTO*/
IF EXISTS (SELECT IDMOV FROM CORPORERMATT..TMOVRATCCU
		   WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						  WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						  AND CODCOLIGADA = @CODCOLIGADA)
		   AND CODCOLIGADA = @CODCOLIGADA)	 	
BEGIN
	DELETE FROM CORPORERMATT..TMOVRATCCU
	WHERE IDMOV = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
				   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
				   AND CODCOLIGADA = @CODCOLIGADA)
	AND CODCOLIGADA = @CODCOLIGADA
END 


/*        A T U A L I Z A C A O      D E       M O V I M E N T O         ( E N T R A D A S )*/ 
----------------------------------------------------------------------------------------------
IF EXISTS (SELECT CODCFO FROM CORPORERMATT..TMOV 
           WHERE CAMPOLIVRE1 = @ID_MOVIMENTO 
           AND CODCOLIGADA = @CODCOLIGADA)
AND @TIPO_MOV   = 'E'                   

BEGIN 
                                
	UPDATE   CORPORERMATT..TMOV
	SET           
		  CODFILIAL                      =  @CODFILIAL                                  --- smallint                                 
		 ,CODLOC                         =  @CODLOC                                     --- varchar(15)                                 
		 ,CODLOCENTREGA                  =  @CODLOCENTREGA                              --- varchar(15)                                 
		 ,CODLOCDESTINO                  =  @CODLOCDESTINO                              --- varchar(15)                                 
		 ,CODCFO                         =  @CODCFO                                     --- varchar(25)                                 
		 ,CODCFONATUREZA                 =  @CODCFONATUREZA                             --- varchar(25)                                 
		 ,NUMEROMOV                      =  @NUMEROMOV                                  --- varchar(35)                                 
		 ,SERIE                          =  @SERIE                                      --- varchar(8)                                 
		 ,CODTMV                         =  @CODTMV                                     --- varchar(10)                                 
		 ,TIPO                           =  @TIPO                                       --- varchar(1)                                 
		 ,STATUS                         =  @STATUS                                     --- varchar(1)                                 
		 ,MOVIMPRESSO                    =  @MOVIMPRESSO                                --- smallint                                 
		 ,DOCIMPRESSO                    =  @DOCIMPRESSO                                --- smallint                                 
		 ,FATIMPRESSA                    =  @FATIMPRESSA                                --- smallint                                 
		 ,DATAEMISSAO                    =  @DATAEMISSAO                                --- datetime                                 
		 ,DATASAIDA                      =  @DATASAIDA                                  --- datetime                                 
		 ,DATAEXTRA1                     =  @DATAEXTRA1                                 --- datetime                                 
		 ,DATAEXTRA2                     =  @DATAEXTRA2                                 --- datetime                                 
		 ,CODRPR                         =  @CODRPR                                     --- varchar(15)                                 
		 ,COMISSAOREPRES                 =  @COMISSAOREPRES                             --- NUMERIC(15,2)                                 
		 ,NORDEM                         =  @NORDEM                                     --- varchar(20)                                 
		 ,CODCPG                         =  @CODCPG                                     --- varchar(5)                                 
		 ,NUMEROTRIBUTOS                 =  @NUMEROTRIBUTOS                             --- smallint                                 
		 ,VALORBRUTO                     =  @VALORBRUTO                                 --- NUMERIC(15,2)                                 
		 ,VALORLIQUIDO                   =  @VALORLIQUIDO                               --- NUMERIC(15,2)                                 
		 ,VALOROUTROS                    =  @VALOROUTROS                                --- NUMERIC(15,2)                                 
		 ,OBSERVACAO                     =  @OBSERVACAO                                 --- varchar(60)                                 
		 ,PERCENTUALFRETE                =  @PERCENTUALFRETE                            --- NUMERIC(15,2)                                 
		 ,VALORFRETE                     =  @VALORFRETE                                 --- NUMERIC(15,2)                                 
		 ,PERCENTUALSEGURO               =  @PERCENTUALSEGURO                           --- NUMERIC(15,2)                                 
		 ,VALORSEGURO                    =  @VALORSEGURO                                --- NUMERIC(15,2)                                 
		 ,PERCENTUALDESC                 =  @PERCENTUALDESC                             --- NUMERIC(15,2)                                 
		 ,VALORDESC                      =  @VALORDESC                                  --- NUMERIC(15,2)                                 
		 ,PERCENTUALDESP                 =  @PERCENTUALDESP                             --- NUMERIC(15,2)                                 
		 ,VALORDESP                      =  @VALORDESP                                  --- NUMERIC(15,2)                                 
		 ,PERCENTUALEXTRA1               =  @PERCENTUALEXTRA1                           --- NUMERIC(15,2)                                 
		 ,VALOREXTRA1                    =  @VALOREXTRA1                                --- NUMERIC(15,2)                                 
		 ,PERCENTUALEXTRA2               =  @PERCENTUALEXTRA2                           --- NUMERIC(15,2)                                 
		 ,VALOREXTRA2                    =  @VALOREXTRA2                                --- NUMERIC(15,2)                                 
		 ,PERCCOMISSAO                   =  @PERCCOMISSAO                               --- NUMERIC(15,2)                                 
		 ,CODMEN                         =  @CODMEN                                     --- varchar(5)                                 
		 ,CODMEN2                        =  @CODMEN2                                    --- varchar(5)                                 
		 ,VIADETRANSPORTE                =  @VIADETRANSPORTE                            --- varchar(15)                                 
		 ,PLACA                          =  @PLACA                                      --- varchar(10)                                 
		 ,CODETDPLACA                    =  @CODETDPLACA                                --- varchar(2)                                 
		 ,PESOLIQUIDO                    =  @PESOLIQUIDO                                --- NUMERIC(15,2)                                 
		 ,PESOBRUTO                      =  @PESOBRUTO                                  --- NUMERIC(15,2)                                 
		 ,MARCA                          =  @MARCA                                      --- varchar(10)                                 
		 ,NUMERO                         =  @NUMERO                                     --- int                                 
		 ,QUANTIDADE                     =  @QUANTIDADE                                 --- int                                 
		 ,ESPECIE                        =  @ESPECIE                                    --- varchar(15)                                 
		 ,CODTB1FAT                      =  @CODTB1FAT                                  --- varchar(10)                                 
		 ,CODTB2FAT                      =  @CODTB2FAT                                  --- varchar(10)                                 
		 ,CODTB3FAT                      =  @CODTB3FAT                                  --- varchar(10)                                 
		 ,CODTB4FAT                      =  @CODTB4FAT                                  --- varchar(10)                                 
		 ,CODTB5FAT                      =  @CODTB5FAT                                  --- varchar(10)                                 
		 ,CODTB1FLX                      =  @CODTB1FLX                                  --- varchar(25)                                 
		 ,CODTB2FLX                      =  @CODTB2FLX                                  --- varchar(25)                                 
		 ,CODTB3FLX                      =  @CODTB3FLX                                  --- varchar(25)                                 
		 ,CODTB4FLX                      =  @CODTB4FLX                                  --- varchar(25)                                 
		 ,CODTB5FLX                      =  @CODTB5FLX                                  --- varchar(25)                                 
		 ,IDMOVRELAC                     =  @IDMOVRELAC                                 --- int                                 
		 ,IDMOVLCTFLUXUS                 =  @IDMOVLCTFLUXUS                             --- int                                 
		 ,IDMOVPEDDESDOBRADO             =  @IDMOVPEDDESDOBRADO                         --- int                                 
		 ,CODMOEVALORLIQUIDO             =  @CODMOEVALORLIQUIDO                         --- varchar(10)                                 
		 ,DATABASEMOV                    =  @DATABASEMOV                                --- datetime                                 
		 ,DATAMOVIMENTO                  =  @DATAMOVIMENTO                              --- datetime                                 
		 ,NUMEROLCTGERADO                =  @NUMEROLCTGERADO                            --- smallint                                 
		 ,GEROUFATURA                    =  @GEROUFATURA                                --- smallint                                 
		 ,NUMEROLCTABERTO                =  @NUMEROLCTABERTO                            --- smallint                                 
		 ,FLAGEXPORTACAO                 =  @FLAGEXPORTACAO                             --- smallint                                 
		 ,EMITEBOLETA                    =  @EMITEBOLETA                                --- varchar(1)                                 
		 ,CODMENDESCONTO                 =  @CODMENDESCONTO                             --- varchar(5)                                 
		 ,CODMENDESPESA                  =  @CODMENDESPESA                              --- varchar(5)                                 
		 ,CODMENFRETE                    =  @CODMENFRETE                                --- varchar(5)                                 
		 ,FRETECIFOUFOB                  =  @FRETECIFOUFOB                              --- smallint                                 
		 ,USADESPFINANC                  =  @USADESPFINANC                              --- smallint                                 
		 ,FLAGEXPORFISC                  =  @FLAGEXPORFISC                              --- smallint                                 
		 ,FLAGEXPORFAZENDA               =  @FLAGEXPORFAZENDA      --- smallint                                 
		 ,VALORADIANTAMENTO              =  @VALORADIANTAMENTO                          --- NUMERIC(15,2)                                 
		 ,CODTRA                         =  @CODTRA                                     --- varchar(5)                                 
		 ,CODTRA2                        =  @CODTRA2                                    --- varchar(5)                                 
		 ,STATUSLIBERACAO                =  @STATUSLIBERACAO                            --- smallint                                 
		 ,CODCFOAUX                      =  @CODCFOAUX                                  --- varchar(25)                                 
		 ,IDLOT                          =  @IDLOT                                      --- int                                 
		 ,ITENSAGRUPADOS                 =  @ITENSAGRUPADOS                             --- smallint                                 
		 ,FLAGIMPRESSAOFAT               =  @FLAGIMPRESSAOFAT                           --- varchar(1)                                 
		 ,DATACANCELAMENTOMOV            =  @DATACANCELAMENTOMOV                        --- datetime                                 
		 ,VALORRECEBIDO                  =  @VALORRECEBIDO                              --- NUMERIC(15,2)                                 
		 ,SEGUNDONUMERO                  =  @SEGUNDONUMERO                              --- varchar(20)                                 
		 ,CODCCUSTO                      =  @CODCCUSTO                                  --- varchar(25)                                 
		 ,CODCXA                         =  @CODCXA                                     --- varchar(10)                                 
		 ,CODVEN1                        =  @CODVEN1                                    --- varchar(16)                                 
		 ,CODVEN2                        =  @CODVEN2                                    --- varchar(16)                                 
		 ,CODVEN3                        =  @CODVEN3                                    --- varchar(16)                                 
		 ,CODVEN4                        =  @CODVEN4                                    --- varchar(16)                                 
		 ,PERCCOMISSAOVEN2               =  @PERCCOMISSAOVEN2                           --- NUMERIC(15,2)                                 
		 ,CODCOLCFO                      =  @CODCOLCFO                                  --- smallint                                 
		 ,CODCOLCFONATUREZA              =  @CODCOLCFONATUREZA                          --- smallint                                 
		 ,CODUSUARIO                     =  @CODUSUARIO                                 --- varchar(20)                                 
		 ,CODFILIALENTREGA               =  @CODFILIALENTREGA                           --- smallint                                 
		 ,CODFILIALDESTINO               =  @CODFILIALDESTINO                           --- smallint                                 
		 ,FLAGAGRUPADOFLUXUS             =  @FLAGAGRUPADOFLUXUS                         --- smallint                                 
		 ,CODCOLCXA                      =  @CODCOLCXA                                  --- SMALLINT                                 
		 ,GERADOPORLOTE                  =  @GERADOPORLOTE                              --- smallint                                 
		 ,CODDEPARTAMENTO                =  @CODDEPARTAMENTO                            --- varchar(25)                                 
		 ,CODCCUSTODESTINO               =  @CODCCUSTODESTINO                           --- varchar(25)                                 
		 ,CODEVENTO                      =  @CODEVENTO                                  --- smallint                                 
		 ,STATUSEXPORTCONT               =  @STATUSEXPORTCONT                           --- smallint                                 
		 ,CODLOTE       =  @CODLOTE                                    --- int                                 
		 ,STATUSCHEQUE                   =  @STATUSCHEQUE                               --- smallint                                 
		 ,DATAENTREGA                    =  @DATAENTREGA                                --- datetime                                 
		 ,DATAPROGRAMACAO                =  @DATAPROGRAMACAO                            --- datetime                                 
		 ,IDNAT                          =  @IDNAT                                      --- int                                 
		 ,IDNAT2                         =  @IDNAT2                                     --- int                                 
		 ,CAMPOLIVRE1                    =  @CAMPOLIVRE1                                --- varchar(100)                                 
		 ,CAMPOLIVRE2                    =  @CAMPOLIVRE2                                --- varchar(100)                                 
		 ,CAMPOLIVRE3                    =  @CAMPOLIVRE3                                --- varchar(100)                                 
		 ,GEROUCONTATRABALHO             =  @GEROUCONTATRABALHO                         --- SMALLINT                                 
		 ,GERADOPORCONTATRABALHO         =  @GERADOPORCONTATRABALHO                     --- SMALLINT                                 
		 ,HORULTIMAALTERACAO             =  @HORULTIMAALTERACAO                         --- datetime                                 
		 ,CODLAF                         =  @CODLAF                                     --- varchar(15)                                 
		 ,DATAFECHAMENTO                 =  @DATAFECHAMENTO                             --- datetime                                 
		 ,NSEQDATAFECHAMENTO             =  @NSEQDATAFECHAMENTO                         --- smallint                                 
		 ,NUMERORECIBO                   =  @NUMERORECIBO                               --- varchar(12)                                 
		 ,IDLOTEPROCESSO                 =  @IDLOTEPROCESSO                             --- int                                 
		 ,IDOBJOF                        =  @IDOBJOF                                    --- varchar(20)                                 
		 ,CODAGENDAMENTO                 =  @CODAGENDAMENTO                             --- int                                 
		 ,CHAPARESP                      =  @CHAPARESP                                  --- VARCHAR(16)                                 
		 ,IDLOTEPROCESSOREFAT            =  @IDLOTEPROCESSOREFAT                        --- int                                 
		 ,INDUSOOBJ                      =  @INDUSOOBJ                                  --- NUMERIC(15,2)                                 
		 ,SUBSERIE                       =  @SUBSERIE                                   --- varchar(8)                                 
		 ,STSCOMPRAS                     =  @STSCOMPRAS                                 --- TINYINT                                 
		 ,CODLOCEXP                      =  @CODLOCEXP                                  --- VARCHAR(15)                                 
		 ,IDCLASSMOV                     =  @IDCLASSMOV                                 --- INT                                 
		 ,CODENTREGA                     =  @CODENTREGA                                 --- VARCHAR(15)                                 
		 ,CODFAIXAENTREGA                =  @CODFAIXAENTREGA                            --- VARCHAR(15)                                 
		 ,DTHENTREGA                     =  @DTHENTREGA                                 --- DATETIME                                 
		 ,CONTABILIZADOPORTOTAL          =  @CONTABILIZADOPORTOTAL                      --- SMALLINT                                 
		 ,CODLAFE                        =  @CODLAFE                                    --- varchar(15)                                 
		 ,IDPRJ             =  @IDPRJ                                      --- int                                 
		 ,NUMEROCUPOM                    =  @NUMEROCUPOM                                --- int                                 
		 ,NUMEROCAIXA                    =  @NUMEROCAIXA                                --- int                                 
		 ,FLAGEFEITOSALDO                =  @FLAGEFEITOSALDO                            --- smallint                                 
		 ,INTEGRADOBONUM                 =  @INTEGRADOBONUM                             --- SMALLINT                                 
		 ,CODMOELANCAMENTO               =  @CODMOELANCAMENTO                           --- varchar(10)                                 
		 ,NAONUMERADO                    =  @NAONUMERADO                                --- varchar(1)                                 
		 ,FLAGPROCESSADO                 =  @FLAGPROCESSADO                             --- SMALLINT                                 
		 ,ABATIMENTOICMS                 =  @ABATIMENTOICMS                             --- NUMERIC(15,2)                                 
		 ,TIPOCONSUMO                    =  @TIPOCONSUMO                                --- smallint                                 
		 ,HORARIOEMISSAO                 =  @HORARIOEMISSAO                             --- datetime                                 
		 ,DATARETORNO                    =  @DATARETORNO                                --- datetime                                 
		 ,USUARIOCRIACAO                 =  @USUARIOCRIACAO                             --- varchar(20)                                 
		 ,DATACRIACAO                    =  @DATACRIACAO                                --- datetime                                 
		 ,IDCONTATOENTREGA               =  @IDCONTATOENTREGA                           --- int                                 
		 ,IDCONTATOCOBRANCA              =  @IDCONTATOCOBRANCA                          --- int                                 
		 ,STATUSSEPARACAO                =  @STATUSSEPARACAO                            --- varchar(1)                                 
		 ,STSEMAIL                       =  @STSEMAIL                                   --- SMALLINT                                 
		 ,VALORFRETECTRC                 =  @VALORFRETECTRC                             --- NUMERIC(15,2)                                 
		 ,PONTOVENDA                     =  @PONTOVENDA                                 --- varchar(10)                                 
		 ,PRAZOENTREGA                   =  @PRAZOENTREGA                               --- int                                 
		 ,VALORBRUTOINTERNO              =  @VALORBRUTOINTERNO                          --- NUMERIC(15,2)                                 
		 ,IDAIDF                         =  @IDAIDF                                     --- smallint                                 
		 ,IDSALDOESTOQUE                 =  @IDSALDOESTOQUE                             --- int                                 
		 ,VINCULADOESTOQUEFL             =  @VINCULADOESTOQUEFL                         --- SMALLINT                                 
		 ,IDREDUCAOZ                     =  @IDREDUCAOZ                                 --- int                                 
		 ,HORASAIDA                      =  @HORASAIDA                                  --- datetime                                 
		 ,CODMUNSERVICO                  =  @CODMUNSERVICO                              --- varchar(20)                                 
		 ,CODETDMUNSERV                  =  @CODETDMUNSERV                              --- varchar(2)                                 
		 ,APROPRIADO                     =  @APROPRIADO                                 --- smallint                                 
		 ,CODIGOSERVICO                  =  @CODIGOSERVICO                              --- varchar(15)                                 
		 ,DATADEDUCAO        =  @DATADEDUCAO                                --- datetime                                 
		 ,CODDIARIO                      =  @CODDIARIO                                  --- varchar(5)                                 
		 ,SEQDIARIO                      =  @SEQDIARIO                                  --- varchar(9)                                 
		 ,SEQDIARIOESTORNO               =  @SEQDIARIOESTORNO                           --- varchar(9)                                 
		 ,INSSEMOUTRAEMPRESA             =  @INSSEMOUTRAEMPRESA                         --- NUMERIC(15,2)                                 
		 ,IDMOVCTRC                      =  @IDMOVCTRC                                  --- int                                 
		 ,DATAPROGRAMACAOANT             =  @DATAPROGRAMACAOANT                         --- datetime                                 
		 ,CODTDO                         =  @CODTDO                                     --- varchar(10)                                 
		 ,VALORDESCCONDICIONAL           =  @VALORDESCCONDICIONAL                       --- NUMERIC(15,2)                                 
		 ,VALORDESPCONDICIONAL           =  @VALORDESPCONDICIONAL                       --- NUMERIC(15,2)                                 
		 ,CODIGOIRRF                     =  @CODIGOIRRF                                 --- varchar(10)                                 
		 ,DEDUCAOIRRF                    =  @DEDUCAOIRRF                                --- NUMERIC(15,2)                                 
		 ,PERCENTBASEINSS                =  @PERCENTBASEINSS                            --- NUMERIC(15,2)                                 
		 ,PERCBASEINSSEMPREGADO          =  @PERCBASEINSSEMPREGADO                      --- NUMERIC(15,2)                                 
		 ,CONTORCAMENTOANTIGO            =  @CONTORCAMENTOANTIGO                        --- SMALLINT                                 
		 ,CODDEPTODESTINO                =  @CODDEPTODESTINO                            --- varchar(25)                                 
		 ,DATACONTABILIZACAO             =  @DATACONTABILIZACAO                         --- datetime                                 
		 ,CODVIATRANSPORTE               =  @CODVIATRANSPORTE                           --- varchar(1)                                 
		 ,VALORSERVICO                   =  @VALORSERVICO                               --- NUMERIC(15,2)                                 
		 ,SEQUENCIALESTOQUE              =  @SEQUENCIALESTOQUE                          --- int                                 
		 ,DISTANCIA                      =  @DISTANCIA                                  --- int                                 
		 ,UNCALCULO                      =  @UNCALCULO                                  --- varchar(1)                                 
		 ,FORMACALCULO                   =  @FORMACALCULO                               --- varchar(1)                                 
		 ,INTEGRADOAUTOMACAO             =  @INTEGRADOAUTOMACAO                         --- smallint                                 
		 ,INTEGRAAPLICACAO               =  @INTEGRAAPLICACAO                           --- char(1)                                 
		 ,CLASSECONSUMO                  =  @CLASSECONSUMO                              --- varchar(1)                                 
		 ,TIPOASSINANTE                  =  @TIPOASSINANTE                              --- varchar(1)                                 
		 ,FASE                           =  @FASE                                       --- varchar(1)                                 
		 ,TIPOUTILIZACAO                 =  @TIPOUTILIZACAO                             --- varchar(1)                                 
		 ,GRUPOTENSAO                    =  @GRUPOTENSAO                                --- varchar(1)                                 
		 ,DATALANCAMENTO                 =  @DATALANCAMENTO                             --- datetime                    
		 ,EXTENPORANEO                   =  @EXTENPORANEO                               --- SMALLINT                                 
		 ,RECIBONFESTATUS                =  @RECIBONFESTATUS                            --- varchar(1)                                 
		 ,RECIBONFETIPO                  =  @RECIBONFETIPO                              --- smallint                                 
		 ,RECIBONFENUMERO                =  @RECIBONFENUMERO                            --- varchar(12)                                 
		 ,RECIBONFESITUACAO              =  @RECIBONFESITUACAO                          --- smallint                                 
		 ,IDMOVCFO                       =  @IDMOVCFO                                   --- int                                 
		 ,OCAUTONOMO                     =  @OCAUTONOMO                                 --- smallint                                 
		 ,VALORMERCADORIAS               =  @VALORMERCADORIAS                           --- NUMERIC(15,2)                                 
		 ,NATUREZAVOLUMES                =  @NATUREZAVOLUMES                            --- varchar(30)                                 
		 ,VOLUMES                        =  @VOLUMES                                    --- varchar(30)                                 
		 ,CRO                            =  @CRO                                        --- smallint                                 
		 ,USARATEIOVALORFIN              =  @USARATEIOVALORFIN                          --- SMALLINT                                 
		 ,RECIBONFESERIE                 =  @RECIBONFESERIE                             --- varchar(5)                                 
		 ,CODCOLCFOORIGEM                =  @CODCOLCFOORIGEM                            --- SMALLINT                                 
		 ,CODCFOORIGEM                   =  @CODCFOORIGEM                               --- varchar(25)                                 
		 ,VALORCTRCARATEAR               =  @VALORCTRCARATEAR                           --- NUMERIC(15,2)                                 
		 ,CODCOLCFOAUX                   =  @CODCOLCFOAUX                               --- smallint                                 
		 ,VRBASEINSSOUTRAEMPRESA         =  @VRBASEINSSOUTRAEMPRESA                     --- NUMERIC(15,2)                                 
		 ,IDCEICFO                       =  @IDCEICFO                                   --- int                                 
		 ,CHAVEACESSONFE                 =  @CHAVEACESSONFE                             --- varchar(44)                                 
		 ,VLRSECCAT                      =  @VLRSECCAT                                  --- NUMERIC(15,2)                                 
		 ,VLRDESPACHO                    =  @VLRDESPACHO                                --- NUMERIC(15,2)                                 
		 ,VLRPEDAGIO                     =  @VLRPEDAGIO                                 --- NUMERIC(15,2)                                 
		 ,VLRFRETEOUTROS                 =  @VLRFRETEOUTROS                             --- NUMERIC(15,2)                                 
		 ,ABATIMENTONAOTRIB              =  @ABATIMENTONAOTRIB                          --- NUMERIC(15,2)                                 
		 ,RATEIOCCUSTODEPTO              =  @RATEIOCCUSTODEPTO                          --- NUMERIC(15,2)                                 
		 ,VALORRATEIOLAN                 =  @VALORRATEIOLAN                             --- NUMERIC(15,2)                                 
		 ,CODCOLCFOTRANSFAT              =  @CODCOLCFOTRANSFAT                          --- SMALLINT                                 
		 ,CODCFOTRANSFAT                 =  @CODCFOTRANSFAT                             --- varchar(25)                                 
		 ,CODUSUARIOAPROVADESC           =  @CODUSUARIOAPROVADESC                       --- varchar(20)   

 
	WHERE   CAMPOLIVRE1	= @ID_MOVIMENTO
	AND     CODCOLIGADA	= @CODCOLIGADA 
	AND     @TIPO_MOV	= 'E';


END



/*        A T U A L I Z A C A O      D E       M O V I M E N T O         ( S A Í D A S ) */ 
------------------------------------------------------------------------------------------
IF EXISTS (SELECT CODCFO FROM CORPORERMATT..TMOV 
           WHERE CAMPOLIVRE1 = @ID_MOVIMENTO 
           AND CODCOLIGADA = @CODCOLIGADA)
AND @TIPO_MOV   = 'S'

BEGIN
 
/*SETA IDMOV CASO JÁ EXISTA O MOVIMENTO NO CORPORE RM*/
	UPDATE   CORPORERMATT..TMOV
	SET           
		 CODFILIAL                      =  @CODFILIAL                                  --- smallint                                 
		,CODLOC                         =  @CODLOC                                     --- varchar(15)                                 
		,CODLOCENTREGA                  =  @CODLOCENTREGA                              --- varchar(15)                                 
		,CODLOCDESTINO                  =  @CODLOCDESTINO                              --- varchar(15)                                 
		,CODCFO                         =  @CODCFO                                     --- varchar(25)                                 
		,CODCFONATUREZA                 =  @CODCFONATUREZA                             --- varchar(25)                                 
		,NUMEROMOV                      =  @NUMEROMOV                                  --- varchar(35)                                 
		,SERIE                          =  @SERIE                                      --- varchar(8)                                 
		,CODTMV                         =  @CODTMV                                     --- varchar(10)                                 
		,TIPO                           =  @TIPO                                       --- varchar(1)                                 
		,STATUS                         =  @STATUS                                     --- varchar(1)                                 
		,MOVIMPRESSO                    =  @MOVIMPRESSO                                --- smallint                                 
		,DOCIMPRESSO                    =  @DOCIMPRESSO                                --- smallint                                 
		,FATIMPRESSA                    =  @FATIMPRESSA                                --- smallint                                 
		,DATAEMISSAO                    =  @DATAEMISSAO                                --- datetime                                 
		,DATASAIDA                      =  @DATASAIDA                                  --- datetime                                 
		,DATAEXTRA1                     =  @DATAEXTRA1                                 --- datetime                                 
		,DATAEXTRA2                     =  @DATAEXTRA2                                 --- datetime                                 
		,CODRPR                         =  @CODRPR                                     --- varchar(15)                                 
		,COMISSAOREPRES                 =  @COMISSAOREPRES                             --- NUMERIC(15,2)                                 
		,NORDEM                         =  @NORDEM                                     --- varchar(20)                                 
		,CODCPG                         =  @CODCPG                                     --- varchar(5)                                 
		,NUMEROTRIBUTOS                 =  @NUMEROTRIBUTOS                             --- smallint                                 
		,VALORBRUTO                     =  @VALORBRUTO                                 --- NUMERIC(15,2)                                 
		,VALORLIQUIDO                   =  @VALORLIQUIDO                               --- NUMERIC(15,2)                                 
		,VALOROUTROS                    =  @VALOROUTROS                                --- NUMERIC(15,2)                                 
		,OBSERVACAO                     =  @OBSERVACAO                                 --- varchar(60)                             
		,PERCENTUALFRETE                =  @PERCENTUALFRETE                            --- NUMERIC(15,2)                                 
		,VALORFRETE                     =  @VALORFRETE                                 --- NUMERIC(15,2)                                 
		,PERCENTUALSEGURO               =  @PERCENTUALSEGURO                           --- NUMERIC(15,2)                                 
		,VALORSEGURO                    =  @VALORSEGURO                                --- NUMERIC(15,2)                                 
		,PERCENTUALDESC                 =  @PERCENTUALDESC                             --- NUMERIC(15,2)                                 
		,VALORDESC                      =  @VALORDESC                                  --- NUMERIC(15,2)                                 
		,PERCENTUALDESP                 =  @PERCENTUALDESP                             --- NUMERIC(15,2)                                 
		,VALORDESP                      =  @VALORDESP                                  --- NUMERIC(15,2)                                 
		,PERCENTUALEXTRA1               =  @PERCENTUALEXTRA1                           --- NUMERIC(15,2)                                 
		,VALOREXTRA1                    =  @VALOREXTRA1                                --- NUMERIC(15,2)                                 
		,PERCENTUALEXTRA2               =  @PERCENTUALEXTRA2                           --- NUMERIC(15,2)                                 
		,VALOREXTRA2                    =  @VALOREXTRA2                                --- NUMERIC(15,2)                                 
		,PERCCOMISSAO                   =  @PERCCOMISSAO                               --- NUMERIC(15,2)                                 
		,CODMEN                         =  @CODMEN                                     --- varchar(5)                                 
		,CODMEN2                        =  @CODMEN2                                    --- varchar(5)                                 
		,VIADETRANSPORTE                =  @VIADETRANSPORTE                            --- varchar(15)                                 
		,PLACA                          =  @PLACA                                      --- varchar(10)                                 
		,CODETDPLACA                    =  @CODETDPLACA                                --- varchar(2)                                 
		,PESOLIQUIDO                    =  @PESOLIQUIDO                                --- NUMERIC(15,2)                                 
		,PESOBRUTO                      =  @PESOBRUTO                                  --- NUMERIC(15,2)                                 
		,MARCA                          =  @MARCA                                      --- varchar(10)                                 
		,NUMERO                         =  @NUMERO                                     --- int                                 
		,QUANTIDADE                     =  @QUANTIDADE                                 --- int                                 
		,ESPECIE                        =  @ESPECIE                                    --- varchar(15)                                 
		,CODTB1FAT                      =  @CODTB1FAT                                  --- varchar(10)                                 
		,CODTB2FAT                      =  @CODTB2FAT                                  --- varchar(10)                                 
		,CODTB3FAT                      =  @CODTB3FAT                                  --- varchar(10)                                 
		,CODTB4FAT                      =  @CODTB4FAT                                  --- varchar(10)                                 
		,CODTB5FAT                      =  @CODTB5FAT                                  --- varchar(10)                                 
		,CODTB1FLX                      =  @CODTB1FLX                                  --- varchar(25)                                 
		,CODTB2FLX      =  @CODTB2FLX                                  --- varchar(25)                                 
		,CODTB3FLX                      =  @CODTB3FLX                                  --- varchar(25)                                 
		,CODTB4FLX                      =  @CODTB4FLX                                  --- varchar(25)                                 
		,CODTB5FLX                      =  @CODTB5FLX                                  --- varchar(25)                                 
		,IDMOVRELAC                     =  @IDMOVRELAC                                 --- int                                 
		,IDMOVLCTFLUXUS                 =  @IDMOVLCTFLUXUS                             --- int                                 
		,IDMOVPEDDESDOBRADO             =  @IDMOVPEDDESDOBRADO                         --- int                                 
		,CODMOEVALORLIQUIDO             =  @CODMOEVALORLIQUIDO                         --- varchar(10)                                 
		,DATABASEMOV                    =  @DATABASEMOV                                --- datetime                                 
		,DATAMOVIMENTO                  =  @DATAMOVIMENTO                              --- datetime                                 
		,NUMEROLCTGERADO                =  @NUMEROLCTGERADO                            --- smallint                                 
		,GEROUFATURA                    =  @GEROUFATURA                                --- smallint                                 
		,NUMEROLCTABERTO                =  @NUMEROLCTABERTO                            --- smallint                                 
		,FLAGEXPORTACAO                 =  @FLAGEXPORTACAO                             --- smallint                                 
		,EMITEBOLETA                    =  @EMITEBOLETA                                --- varchar(1)                                 
		,CODMENDESCONTO                 =  @CODMENDESCONTO                             --- varchar(5)                                 
		,CODMENDESPESA                  =  @CODMENDESPESA                              --- varchar(5)                                 
		,CODMENFRETE                    =  @CODMENFRETE                                --- varchar(5)                                 
		,FRETECIFOUFOB                  =  @FRETECIFOUFOB                              --- smallint                                 
		,USADESPFINANC                  =  @USADESPFINANC                              --- smallint                                 
		,FLAGEXPORFISC                  =  @FLAGEXPORFISC                              --- smallint                                 
		,FLAGEXPORFAZENDA               =  @FLAGEXPORFAZENDA                           --- smallint                                 
		,VALORADIANTAMENTO              =  @VALORADIANTAMENTO                          --- NUMERIC(15,2)                                 
		,CODTRA                         =  @CODTRA                                     --- varchar(5)                                 
		,CODTRA2                        =  @CODTRA2                                    --- varchar(5)                                 
		,STATUSLIBERACAO                =  @STATUSLIBERACAO                            --- smallint                                 
		,CODCFOAUX                      =  @CODCFOAUX                                  --- varchar(25)                                 
		,IDLOT                          =  @IDLOT                                      --- int                                 
		,ITENSAGRUPADOS                 =  @ITENSAGRUPADOS                             --- smallint                                 
		,FLAGIMPRESSAOFAT               =  @FLAGIMPRESSAOFAT                           --- varchar(1)                                 
		,DATACANCELAMENTOMOV            =  @DATACANCELAMENTOMOV                        --- datetime                                 
		,VALORRECEBIDO                  =  @VALORRECEBIDO                              --- NUMERIC(15,2)                                 
		,SEGUNDONUMERO                  =  @SEGUNDONUMERO                              --- varchar(20)                                 
		,CODCCUSTO                      =  @CODCCUSTO                                  --- varchar(25)                                 
		,CODCXA                         =  @CODCXA                                     --- varchar(10)                                 
		,CODVEN1                        =  @CODVEN1                                    --- varchar(16)                                 
		,CODVEN2                        =  @CODVEN2                                    --- varchar(16)                                 
		,CODVEN3                        =  @CODVEN3                                    --- varchar(16)                                 
		,CODVEN4                        =  @CODVEN4                                    --- varchar(16)                                 
		,PERCCOMISSAOVEN2               =  @PERCCOMISSAOVEN2                           --- NUMERIC(15,2)                                 
		,CODCOLCFO                      =  @CODCOLCFO                                  --- smallint                                 
		,CODCOLCFONATUREZA              =  @CODCOLCFONATUREZA                          --- smallint                                 
		,CODUSUARIO                     =  @CODUSUARIO                                 --- varchar(20)                                 
		,CODFILIALENTREGA               =  @CODFILIALENTREGA                           --- smallint                                 
		,CODFILIALDESTINO               =  @CODFILIALDESTINO                           --- smallint                                 
		,FLAGAGRUPADOFLUXUS             =  @FLAGAGRUPADOFLUXUS                         --- smallint                                 
		,CODCOLCXA                      =  @CODCOLCXA                                  --- SMALLINT                                 
		,GERADOPORLOTE                  =  @GERADOPORLOTE                              --- smallint                                 
		,CODDEPARTAMENTO                =  @CODDEPARTAMENTO                            --- varchar(25)                                 
		,CODCCUSTODESTINO               =  @CODCCUSTODESTINO                           --- varchar(25)                                 
		,CODEVENTO                      =  @CODEVENTO                                  --- smallint                                 
		,STATUSEXPORTCONT               =  @STATUSEXPORTCONT                           --- smallint                                 
		,CODLOTE                        =  @CODLOTE                                    --- int                                 
		,STATUSCHEQUE                   =  @STATUSCHEQUE                               --- smallint                                 
		,DATAENTREGA                    =  @DATAENTREGA                                --- datetime                                 
		,DATAPROGRAMACAO                =  @DATAPROGRAMACAO                            --- datetime                                 
		,IDNAT                          =  @IDNAT                                      --- int                                 
		,IDNAT2                         =  @IDNAT2                                     --- int                                 
		,CAMPOLIVRE1                    =  @CAMPOLIVRE1                                --- varchar(100)                                 
		,CAMPOLIVRE2                    =  @CAMPOLIVRE2                                --- varchar(100)                                 
		,CAMPOLIVRE3                    =  @CAMPOLIVRE3                                --- varchar(100)                                 
		,GEROUCONTATRABALHO             =  @GEROUCONTATRABALHO                         --- SMALLINT                                 
		,GERADOPORCONTATRABALHO         =  @GERADOPORCONTATRABALHO                     --- SMALLINT                                 
		,HORULTIMAALTERACAO             =  @HORULTIMAALTERACAO                         --- datetime                                 
		,CODLAF                         =  @CODLAF                                     --- varchar(15)                                 
		,DATAFECHAMENTO                 =  @DATAFECHAMENTO                             --- datetime                                 
		,NSEQDATAFECHAMENTO             =  @NSEQDATAFECHAMENTO                         --- smallint                                 
		,NUMERORECIBO                   =  @NUMERORECIBO                               --- varchar(12)                                 
		,IDLOTEPROCESSO                 =  @IDLOTEPROCESSO                             --- int                                 
		,IDOBJOF                        =  @IDOBJOF                                    --- varchar(20)                                 
		,CODAGENDAMENTO                 =  @CODAGENDAMENTO                             --- int                                 
		,CHAPARESP                      =  @CHAPARESP                                  --- VARCHAR(16)                                 
		,IDLOTEPROCESSOREFAT            =  @IDLOTEPROCESSOREFAT                        --- int                                 
		,INDUSOOBJ                      =  @INDUSOOBJ                                  --- NUMERIC(15,2)                                 
		,SUBSERIE                       =  @SUBSERIE                                   --- varchar(8)                                 
		,STSCOMPRAS                     =  @STSCOMPRAS                                 --- TINYINT                                 
		,CODLOCEXP                      =  @CODLOCEXP                                  --- VARCHAR(15)                                 
		,IDCLASSMOV                     =  @IDCLASSMOV                                 --- INT                                 
		,CODENTREGA                     =  @CODENTREGA                                 --- VARCHAR(15)                                 
		,CODFAIXAENTREGA                =  @CODFAIXAENTREGA                            --- VARCHAR(15)                                 
		,DTHENTREGA                     =  @DTHENTREGA                                 --- DATETIME                                 
		,CONTABILIZADOPORTOTAL          =  @CONTABILIZADOPORTOTAL                      --- SMALLINT                                 
		,CODLAFE                        =  @CODLAFE                                    --- varchar(15)                                 
		,IDPRJ                          =  @IDPRJ                                      --- int                                 
		,NUMEROCUPOM                    =  @NUMEROCUPOM                                --- int                                 
		,NUMEROCAIXA                    =  @NUMEROCAIXA                                --- int                                 
		,FLAGEFEITOSALDO                =  @FLAGEFEITOSALDO                            --- smallint                                 
		,INTEGRADOBONUM                 =  @INTEGRADOBONUM                             --- SMALLINT                                 
		,CODMOELANCAMENTO               =  @CODMOELANCAMENTO                           --- varchar(10)                                 
		,NAONUMERADO                    =  @NAONUMERADO                                --- varchar(1)                                 
		,FLAGPROCESSADO                 =  @FLAGPROCESSADO                             --- SMALLINT                                 
		,ABATIMENTOICMS                 =  @ABATIMENTOICMS                             --- NUMERIC(15,2)                                 
		,TIPOCONSUMO                    =  @TIPOCONSUMO                                --- smallint                                 
		,HORARIOEMISSAO                 =  @HORARIOEMISSAO                --- datetime                                 
		,DATARETORNO                    =  @DATARETORNO                                --- datetime                                 
		,USUARIOCRIACAO                 =  @USUARIOCRIACAO                             --- varchar(20)                                 
		,DATACRIACAO                    =  @DATACRIACAO                                --- datetime                                 
		,IDCONTATOENTREGA               =  @IDCONTATOENTREGA                           --- int                                 
		,IDCONTATOCOBRANCA              =  @IDCONTATOCOBRANCA                          --- int                                 
		,STATUSSEPARACAO                =  @STATUSSEPARACAO                            --- varchar(1)                                 
		,STSEMAIL                       =  @STSEMAIL                                   --- SMALLINT                                 
		,VALORFRETECTRC                 =  @VALORFRETECTRC                             --- NUMERIC(15,2)                                 
		,PONTOVENDA                     =  @PONTOVENDA                                 --- varchar(10)                                 
		,PRAZOENTREGA                   =  @PRAZOENTREGA                               --- int                                 
		,VALORBRUTOINTERNO              =  @VALORBRUTOINTERNO                          --- NUMERIC(15,2)                                 
		,IDAIDF                         =  @IDAIDF                                     --- smallint                                 
		,IDSALDOESTOQUE                 =  @IDSALDOESTOQUE                             --- int                                 
		,VINCULADOESTOQUEFL             =  @VINCULADOESTOQUEFL                         --- SMALLINT                                 
		,IDREDUCAOZ                     =  @IDREDUCAOZ                                 --- int                                 
		,HORASAIDA                      =  @HORASAIDA                                  --- datetime                                 
		,CODMUNSERVICO                  =  @CODMUNSERVICO                              --- varchar(20)                                 
		,CODETDMUNSERV                  =  @CODETDMUNSERV                              --- varchar(2)                                 
		,APROPRIADO                     =  @APROPRIADO                                 --- smallint                                 
		,CODIGOSERVICO                  =  @CODIGOSERVICO                              --- varchar(15)                                 
		,DATADEDUCAO                    =  @DATADEDUCAO                                --- datetime                                 
		,CODDIARIO                      =  @CODDIARIO                                  --- varchar(5)                                 
		,SEQDIARIO                      =  @SEQDIARIO                                  --- varchar(9)                                 
		,SEQDIARIOESTORNO               =  @SEQDIARIOESTORNO                           --- varchar(9)                                 
		,INSSEMOUTRAEMPRESA             =  @INSSEMOUTRAEMPRESA                         --- NUMERIC(15,2)                                 
		,IDMOVCTRC                      =  @IDMOVCTRC                                  --- int                                 
		,DATAPROGRAMACAOANT             =  @DATAPROGRAMACAOANT                         --- datetime                                 
		,CODTDO                         =  @CODTDO                                     --- varchar(10)                                 
		,VALORDESCCONDICIONAL           =  @VALORDESCCONDICIONAL                       --- NUMERIC(15,2)                                 
		,VALORDESPCONDICIONAL           =  @VALORDESPCONDICIONAL                       --- NUMERIC(15,2)                                 
		,CODIGOIRRF                     =  @CODIGOIRRF    --- varchar(10)                                 
		,DEDUCAOIRRF                    =  @DEDUCAOIRRF                                --- NUMERIC(15,2)                                 
		,PERCENTBASEINSS                =  @PERCENTBASEINSS                            --- NUMERIC(15,2)                                 
		,PERCBASEINSSEMPREGADO          =  @PERCBASEINSSEMPREGADO                      --- NUMERIC(15,2)                                 
		,CONTORCAMENTOANTIGO            =  @CONTORCAMENTOANTIGO                        --- SMALLINT                                 
		,CODDEPTODESTINO                =  @CODDEPTODESTINO                            --- varchar(25)                                 
		,DATACONTABILIZACAO             =  @DATACONTABILIZACAO                         --- datetime                                 
		,CODVIATRANSPORTE               =  @CODVIATRANSPORTE                           --- varchar(1)                                 
		,VALORSERVICO                   =  @VALORSERVICO                               --- NUMERIC(15,2)                                 
		,SEQUENCIALESTOQUE              =  @SEQUENCIALESTOQUE                          --- int                                 
		,DISTANCIA                      =  @DISTANCIA                                  --- int                                 
		,UNCALCULO                      =  @UNCALCULO                                  --- varchar(1)                                 
		,FORMACALCULO                   =  @FORMACALCULO                               --- varchar(1)                                 
		,INTEGRADOAUTOMACAO             =  @INTEGRADOAUTOMACAO                         --- smallint                                 
		,INTEGRAAPLICACAO               =  @INTEGRAAPLICACAO                           --- char(1)                                 
		,CLASSECONSUMO                  =  @CLASSECONSUMO                              --- varchar(1)                                 
		,TIPOASSINANTE                  =  @TIPOASSINANTE                              --- varchar(1)                                 
		,FASE                           =  @FASE                                       --- varchar(1)                                 
		,TIPOUTILIZACAO                 =  @TIPOUTILIZACAO                             --- varchar(1)                                 
		,GRUPOTENSAO                    =  @GRUPOTENSAO                                --- varchar(1)                                 
		,DATALANCAMENTO                 =  @DATALANCAMENTO                             --- datetime                                 
		,EXTENPORANEO                   =  @EXTENPORANEO                               --- SMALLINT                                 
		,RECIBONFESTATUS                =  @RECIBONFESTATUS                            --- varchar(1)                                 
		,RECIBONFETIPO                  =  @RECIBONFETIPO                              --- smallint                                 
		,RECIBONFENUMERO                =  @RECIBONFENUMERO                            --- varchar(12)                                 
		,RECIBONFESITUACAO              =  @RECIBONFESITUACAO                          --- smallint                                 
		,IDMOVCFO                       =  @IDMOVCFO                                   --- int                                 
		,OCAUTONOMO                     =  @OCAUTONOMO                                 --- smallint                                 
		,VALORMERCADORIAS               =  @VALORMERCADORIAS                           --- NUMERIC(15,2)                                 
		,NATUREZAVOLUMES                =  @NATUREZAVOLUMES                            --- varchar(30)                                 
		,VOLUMES                        =  @VOLUMES                                    --- varchar(30)                                 
		,CRO                            =  @CRO              --- smallint                                 
		,USARATEIOVALORFIN              =  @USARATEIOVALORFIN                          --- SMALLINT                                 
		,RECIBONFESERIE                 =  @RECIBONFESERIE                             --- varchar(5)                                 
		,CODCOLCFOORIGEM                =  @CODCOLCFOORIGEM                            --- SMALLINT                                 
		,CODCFOORIGEM                   =  @CODCFOORIGEM                               --- varchar(25)                                 
		,VALORCTRCARATEAR               =  @VALORCTRCARATEAR                           --- NUMERIC(15,2)                                 
		,CODCOLCFOAUX                   =  @CODCOLCFOAUX                               --- smallint                                 
		,VRBASEINSSOUTRAEMPRESA         =  @VRBASEINSSOUTRAEMPRESA                     --- NUMERIC(15,2)                                 
		,IDCEICFO                       =  @IDCEICFO                                   --- int                                 
		,CHAVEACESSONFE                 =  @CHAVEACESSONFE                             --- varchar(44)                                 
		,VLRSECCAT                      =  @VLRSECCAT                                  --- NUMERIC(15,2)                                 
		,VLRDESPACHO                    =  @VLRDESPACHO                                --- NUMERIC(15,2)                                 
		,VLRPEDAGIO                     =  @VLRPEDAGIO                                 --- NUMERIC(15,2)                                 
		,VLRFRETEOUTROS                 =  @VLRFRETEOUTROS                             --- NUMERIC(15,2)                                 
		,ABATIMENTONAOTRIB              =  @ABATIMENTONAOTRIB                          --- NUMERIC(15,2)                                 
		,RATEIOCCUSTODEPTO              =  @RATEIOCCUSTODEPTO                          --- NUMERIC(15,2)                                 
		,VALORRATEIOLAN                 =  @VALORRATEIOLAN                             --- NUMERIC(15,2)                                 
		,CODCOLCFOTRANSFAT              =  @CODCOLCFOTRANSFAT                          --- SMALLINT                                 
		,CODCFOTRANSFAT                 =  @CODCFOTRANSFAT                             --- varchar(25)                                 
		,CODUSUARIOAPROVADESC           =  @CODUSUARIOAPROVADESC                       --- varchar(20)   


	WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
	AND CODCOLIGADA = @CODCOLIGADA
	AND @TIPO_MOV = 'S';

END


/*        I N S E R T         D E         M O V I M E N T O         ( E N T R A D A S )*/ 
-----------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT CODCFO FROM CORPORERMATT..TMOV
			   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
			   AND CODCOLIGADA = @CODCOLIGADA )                          
AND @TIPO_MOV = 'E'
                 
BEGIN 

	-- JAILSON (11/07/2011)
	IF NOT EXISTS (SELECT VALAUTOINC FROM CORPORERMATT..GAUTOINC
						 WHERE CODAUTOINC = 'IDMOV' 
						 AND CODCOLIGADA=@CODCOLIGADA)
	BEGIN
		INSERT INTO CORPORERMATT..GAUTOINC
		(CODCOLIGADA,CODSISTEMA,CODAUTOINC,VALAUTOINC)
		VALUES
		(@CODCOLIGADA,'T','IDMOV',0)
	END

	DECLARE @IDMOV_IE INT;
	SELECT  @IDMOV_IE = (SELECT VALAUTOINC+1 FROM CORPORERMATT..GAUTOINC
						 WHERE CODAUTOINC = 'IDMOV' 
						 AND CODCOLIGADA=@CODCOLIGADA); 

	

	INSERT INTO [CORPORERMATT].[dbo].[TMOV]
		([CODCOLIGADA]
		,[IDMOV]
		,[CODFILIAL]
		,[CODLOC]
		,[CODLOCENTREGA]
		,[CODLOCDESTINO]
		,[CODCFO]
		,[CODCFONATUREZA]
		,[NUMEROMOV]
		,[SERIE]
		,[CODTMV]
		,[TIPO]
		,[STATUS]
		,[MOVIMPRESSO]
		,[DOCIMPRESSO]
		,[FATIMPRESSA]
		,[DATAEMISSAO]
		,[DATASAIDA]
		,[DATAEXTRA1]
		,[DATAEXTRA2]
		,[CODRPR]
		,[COMISSAOREPRES]
		,[NORDEM]
		,[CODCPG]
		,[NUMEROTRIBUTOS]
		,[VALORBRUTO]
		,[VALORLIQUIDO]
		,[VALOROUTROS]
		,[OBSERVACAO]
		,[PERCENTUALFRETE]
		,[VALORFRETE]
		,[PERCENTUALSEGURO]
		,[VALORSEGURO]
		,[PERCENTUALDESC]
		,[VALORDESC]
		,[PERCENTUALDESP]
		,[VALORDESP]
		,[PERCENTUALEXTRA1]
		,[VALOREXTRA1]
		,[PERCENTUALEXTRA2]
		,[VALOREXTRA2]
		,[PERCCOMISSAO]
		,[CODMEN]
		,[CODMEN2]
		,[VIADETRANSPORTE]
		,[PLACA]
		,[CODETDPLACA]
		,[PESOLIQUIDO]
		,[PESOBRUTO]
		,[MARCA]
		,[NUMERO]
		,[QUANTIDADE]
		,[ESPECIE]
		,[CODTB1FAT]
		,[CODTB2FAT]
		,[CODTB3FAT]
		,[CODTB4FAT]
		,[CODTB5FAT]
		,[CODTB1FLX]
		,[CODTB2FLX]
		,[CODTB3FLX]
		,[CODTB4FLX]
		,[CODTB5FLX]
		,[IDMOVRELAC]
		,[IDMOVLCTFLUXUS]
		,[IDMOVPEDDESDOBRADO]
		,[CODMOEVALORLIQUIDO]
		,[DATABASEMOV]
		,[DATAMOVIMENTO]
		,[NUMEROLCTGERADO]
		,[GEROUFATURA]
		,[NUMEROLCTABERTO]
		,[FLAGEXPORTACAO]
		,[EMITEBOLETA]
		,[CODMENDESCONTO]
		,[CODMENDESPESA]
		,[CODMENFRETE]
		,[FRETECIFOUFOB]
		,[USADESPFINANC]
		,[FLAGEXPORFISC]
		,[FLAGEXPORFAZENDA]
		,[VALORADIANTAMENTO]
		,[CODTRA]
		,[CODTRA2]
		,[STATUSLIBERACAO]
		,[CODCFOAUX]
		,[IDLOT]
		,[ITENSAGRUPADOS]
		,[FLAGIMPRESSAOFAT]
		,[DATACANCELAMENTOMOV]
		,[VALORRECEBIDO]
		,[SEGUNDONUMERO]
		,[CODCCUSTO]
		,[CODCXA]
		,[CODVEN1]
		,[CODVEN2]
		,[CODVEN3]
		,[CODVEN4]
		,[PERCCOMISSAOVEN2]
		,[CODCOLCFO]
		,[CODCOLCFONATUREZA]
		,[CODUSUARIO]
		,[CODFILIALENTREGA]
		,[CODFILIALDESTINO]
		,[FLAGAGRUPADOFLUXUS]
		,[CODCOLCXA]
		,[GERADOPORLOTE]
		,[CODDEPARTAMENTO]
		,[CODCCUSTODESTINO]
		,[CODEVENTO]
		,[STATUSEXPORTCONT]
		,[CODLOTE]
		,[STATUSCHEQUE]
		,[DATAENTREGA]
		,[DATAPROGRAMACAO]
		,[IDNAT]
		,[IDNAT2]
		,[CAMPOLIVRE1]
		,[CAMPOLIVRE2]
		,[CAMPOLIVRE3]
		,[GEROUCONTATRABALHO]
		,[GERADOPORCONTATRABALHO]
		,[HORULTIMAALTERACAO]
		,[CODLAF]
		,[DATAFECHAMENTO]
		,[NSEQDATAFECHAMENTO]
		,[NUMERORECIBO]
		,[IDLOTEPROCESSO]
		,[IDOBJOF]
		,[CODAGENDAMENTO]
		,[CHAPARESP]
		,[IDLOTEPROCESSOREFAT]

		,[INDUSOOBJ]
		,[SUBSERIE]
		,[STSCOMPRAS]
		,[CODLOCEXP]
		,[IDCLASSMOV]
		,[CODENTREGA]
		,[CODFAIXAENTREGA]
		,[DTHENTREGA]
		,[CONTABILIZADOPORTOTAL]
		,[CODLAFE]
		,[IDPRJ]
		,[NUMEROCUPOM]
		,[NUMEROCAIXA]
		,[FLAGEFEITOSALDO]
		,[INTEGRADOBONUM]
		,[CODMOELANCAMENTO]
		,[NAONUMERADO]
		,[FLAGPROCESSADO]
		,[ABATIMENTOICMS]
		,[TIPOCONSUMO]
		,[HORARIOEMISSAO]
		,[DATARETORNO]
		,[USUARIOCRIACAO]
		,[DATACRIACAO]
		,[IDCONTATOENTREGA]
		,[IDCONTATOCOBRANCA]
		,[STATUSSEPARACAO]
		,[STSEMAIL]
		,[VALORFRETECTRC]
		,[PONTOVENDA]
		,[PRAZOENTREGA]
		,[VALORBRUTOINTERNO]
		,[IDAIDF]
		,[IDSALDOESTOQUE]
		,[VINCULADOESTOQUEFL]
		,[IDREDUCAOZ]
		,[HORASAIDA]
		,[CODMUNSERVICO]
		,[CODETDMUNSERV]
		,[APROPRIADO]
		,[CODIGOSERVICO]
		,[DATADEDUCAO]
		,[CODDIARIO]
		,[SEQDIARIO]
		,[SEQDIARIOESTORNO]
		,[INSSEMOUTRAEMPRESA]
		,[IDMOVCTRC]
		,[DATAPROGRAMACAOANT]
		,[CODTDO]
		,[VALORDESCCONDICIONAL]
		,[VALORDESPCONDICIONAL]
		,[CODIGOIRRF]
		,[DEDUCAOIRRF]
		,[PERCENTBASEINSS]
		,[PERCBASEINSSEMPREGADO]
		,[CONTORCAMENTOANTIGO]
		,[CODDEPTODESTINO]
		,[DATACONTABILIZACAO]
		,[CODVIATRANSPORTE]
		,[VALORSERVICO]
		,[SEQUENCIALESTOQUE]
		,[DISTANCIA]
		,[UNCALCULO]
		,[FORMACALCULO]
		,[INTEGRADOAUTOMACAO]
		,[INTEGRAAPLICACAO]
		,[CLASSECONSUMO]
		,[TIPOASSINANTE]
		,[FASE]
		,[TIPOUTILIZACAO]
		,[GRUPOTENSAO]
		,[DATALANCAMENTO]
		,[EXTENPORANEO]
		,[RECIBONFESTATUS]
		,[RECIBONFETIPO]
		,[RECIBONFENUMERO]
		,[RECIBONFESITUACAO]
		,[IDMOVCFO]
		,[OCAUTONOMO]
		,[VALORMERCADORIAS]
		,[NATUREZAVOLUMES]
		,[VOLUMES]
		,[CRO]
		,[USARATEIOVALORFIN]
		,[RECIBONFESERIE]
		,[CODCOLCFOORIGEM]
		,[CODCFOORIGEM]
		,[VALORCTRCARATEAR]
		,[CODCOLCFOAUX]
		,[VRBASEINSSOUTRAEMPRESA]
		,[IDCEICFO]
		,[CHAVEACESSONFE]
		,[VLRSECCAT]
		,[VLRDESPACHO]
		,[VLRPEDAGIO]
		,[VLRFRETEOUTROS]
		,[ABATIMENTONAOTRIB]
		,[RATEIOCCUSTODEPTO]
		,[VALORRATEIOLAN]
		,[CODCOLCFOTRANSFAT]
		,[CODCFOTRANSFAT]
		,[CODUSUARIOAPROVADESC]
		,[IDINTEGRACAO]
		,[STATUSANTERIOR]
		,[VALORBRUTOORIG]
		,[VALORLIQUIDOORIG]
		,[VALOROUTROSORIG]
		,[VALORRATEIOLANORIG]
		,[DATAPROCESSAMENTO]
		,[IDNATFRETE]
		,[IDOPERACAO]
		,[RECCREATEDBY]
		,[RECCREATEDON]
		,[RECMODIFIEDBY]
		,[RECMODIFIEDON]
	)
           
	VALUES
	(
		 @CODCOLIGADA                                ---SMALLINT      
		,@IDMOV_IE                                   ---int  IDMOV                                 
		,@CODFILIAL                                  ---smallint                                 
		,@CODLOC                                     ---varchar(15)                                 
		,@CODLOCENTREGA                              ---varchar(15)                                 
		,@CODLOCDESTINO                              ---varchar(15)                                 
		,@CODCFO                                     ---varchar(25)                                 
		,@CODCFONATUREZA                             ---varchar(25)                                 
		,@NUMEROMOV                                  ---varchar(35)                                 
		,@SERIE                                      ---varchar(8)                                 
		,@CODTMV                                     ---varchar(10)                                 
		,@TIPO                                       ---varchar(1)                                 
		,@STATUS                                     ---varchar(1)                                 
		,@MOVIMPRESSO                                ---smallint                                 
		,@DOCIMPRESSO                                ---smallint                                 
		,@FATIMPRESSA                                ---smallint                                 
		,@DATAEMISSAO                                ---datetime                                 
		,@DATASAIDA                                  ---datetime                                 
		,@DATAEXTRA1                                 ---datetime                                 
		,@DATAEXTRA2                                 ---datetime                                 
		,@CODRPR                                     ---varchar(15)                                 
		,@COMISSAOREPRES                             ---NUMERIC(15,2)                                 
		,@NORDEM                                     ---varchar(20)                                 
		,@CODCPG                                     ---varchar(5)                                 
		,@NUMEROTRIBUTOS                             ---smallint                                 
		,@VALORBRUTO                                 ---NUMERIC(15,2)                                 
		,@VALORLIQUIDO                               ---NUMERIC(15,2)                                 
		,@VALOROUTROS                                ---NUMERIC(15,2)                                 
		,@OBSERVACAO                                 ---varchar(60)                                 
		,@PERCENTUALFRETE                            ---NUMERIC(15,2)                                 
		,@VALORFRETE                                 ---NUMERIC(15,2)                                 
		,@PERCENTUALSEGURO                           ---NUMERIC(15,2)                                 
		,@VALORSEGURO                                ---NUMERIC(15,2)                                 
		,@PERCENTUALDESC                             ---NUMERIC(15,2)                                 
		,@VALORDESC                                  ---NUMERIC(15,2)                                 
		,@PERCENTUALDESP                             ---NUMERIC(15,2)                                 
		,@VALORDESP                                  ---NUMERIC(15,2)                                 
		,@PERCENTUALEXTRA1                           ---NUMERIC(15,2)                                 
		,@VALOREXTRA1                                ---NUMERIC(15,2)                                 
		,@PERCENTUALEXTRA2                           ---NUMERIC(15,2)                                 
		,@VALOREXTRA2                                ---NUMERIC(15,2)                                 
		,@PERCCOMISSAO                               ---NUMERIC(15,2)                                 
		,@CODMEN                                     ---varchar(5)                                 
		,@CODMEN2                                    ---varchar(5)                                 
		,@VIADETRANSPORTE                            ---varchar(15)                                 
		,@PLACA                                      ---varchar(10)                                 
		,@CODETDPLACA                                ---varchar(2)                                 
		,@PESOLIQUIDO                                ---NUMERIC(15,2)                                 
		,@PESOBRUTO                                  ---NUMERIC(15,2)                                 
		,@MARCA                                      ---varchar(10)                                 
		,@NUMERO                                     ---int                                 
		,@QUANTIDADE                                 ---int                                 
		,@ESPECIE                                    ---varchar(15)                                 
		,@CODTB1FAT                                  ---varchar(10)                                 
		,@CODTB2FAT                                  ---varchar(10)                                 
		,@CODTB3FAT                                  ---varchar(10)                                 
		,@CODTB4FAT                                  ---varchar(10)                                 
		,@CODTB5FAT                                  ---varchar(10)                                 
		,@CODTB1FLX                                  ---varchar(25)                                 
		,@CODTB2FLX                                  ---varchar(25)                                 
		,@CODTB3FLX                                  ---varchar(25)                                 
		,@CODTB4FLX                                  ---varchar(25)                                 
		,@CODTB5FLX                                  ---varchar(25)                                 
		,@IDMOVRELAC                                 ---int                                 
		,@IDMOVLCTFLUXUS                             ---int                                 
		,@IDMOVPEDDESDOBRADO                         ---int                                 
		,@CODMOEVALORLIQUIDO                         ---varchar(10)                                 
		,@DATABASEMOV                                ---datetime                                 
		,@DATAMOVIMENTO                              ---datetime                                 
		,@NUMEROLCTGERADO                            ---smallint                                 
		,@GEROUFATURA                                ---smallint                                 
		,@NUMEROLCTABERTO                            ---smallint                                 
		,@FLAGEXPORTACAO                             ---smallint                                 
		,@EMITEBOLETA                                ---varchar(1)                                 
		,@CODMENDESCONTO                             ---varchar(5)                                 
		,@CODMENDESPESA                              ---varchar(5)                                 
		,@CODMENFRETE                                ---varchar(5)                                 
		,@FRETECIFOUFOB                              ---smallint                                 
		,@USADESPFINANC                              ---smallint                                 
		,@FLAGEXPORFISC                              ---smallint                                 
		,@FLAGEXPORFAZENDA                           ---smallint                                 
		,@VALORADIANTAMENTO     ---NUMERIC(15,2)                                 
		,@CODTRA                                     ---varchar(5)                                 
		,@CODTRA2                                    ---varchar(5)                                 
		,@STATUSLIBERACAO                            ---smallint                                 
		,@CODCFOAUX                                  ---varchar(25)                                 
		,@IDLOT                                      ---int                                 
		,@ITENSAGRUPADOS                             ---smallint                                 
		,@FLAGIMPRESSAOFAT                           ---varchar(1)                                 
		,@DATACANCELAMENTOMOV                        ---datetime                                 
		,@VALORRECEBIDO                              ---NUMERIC(15,2)                                 
		,@SEGUNDONUMERO                              ---varchar(20)                                 
		,@CODCCUSTO                                  ---varchar(25)                                 
		,@CODCXA                                     ---varchar(10)                                 
		,@CODVEN1                                    ---varchar(16)                                 
		,@CODVEN2                                    ---varchar(16)                                 
		,@CODVEN3                                    ---varchar(16)                                 
		,@CODVEN4                                    ---varchar(16)                                 
		,@PERCCOMISSAOVEN2                           ---NUMERIC(15,2)                                 
		,@CODCOLCFO                                  ---smallint                                 
		,@CODCOLCFONATUREZA                          ---smallint                                 
		,@CODUSUARIO                                 ---varchar(20)                                 
		,@CODFILIALENTREGA                           ---smallint                                 
		,@CODFILIALDESTINO                           ---smallint                                 
		,@FLAGAGRUPADOFLUXUS                         ---smallint                                 
		,@CODCOLCXA                                  ---SMALLINT                                 
		,@GERADOPORLOTE                              ---smallint                                 
		,@CODDEPARTAMENTO                            ---varchar(25)                                 
		,@CODCCUSTODESTINO                           ---varchar(25)                                 
		,@CODEVENTO                                  ---smallint                                 
		,@STATUSEXPORTCONT                           ---smallint                                 
		,@CODLOTE                                    ---int                                 
		,@STATUSCHEQUE                               ---smallint                                 
		,@DATAENTREGA                                ---datetime                                 
		,@DATAPROGRAMACAO                            ---datetime                                 
		,@IDNAT                                      ---int                                 
		,@IDNAT2                                     ---int                                 
		,@CAMPOLIVRE1                                ---varchar(100)                                 
		,@CAMPOLIVRE2                                ---varchar(100)                                 
		,@CAMPOLIVRE3                                ---varchar(100)                                 
		,@GEROUCONTATRABALHO                         ---SMALLINT                                 
		,@GERADOPORCONTATRABALHO                     ---SMALLINT                                 
		,@HORULTIMAALTERACAO                         ---datetime                                 
		,@CODLAF                                     ---varchar(15)    
		,@DATAFECHAMENTO                             ---datetime                                 
		,@NSEQDATAFECHAMENTO                         ---smallint                                 
		,@NUMERORECIBO                               ---varchar(12)                                 
		,@IDLOTEPROCESSO                             ---int                                 
		,@IDOBJOF                                    ---varchar(20)                                 
		,@CODAGENDAMENTO                             ---int                                 
		,@CHAPARESP                                  ---VARCHAR(16)                                 
		,@IDLOTEPROCESSOREFAT                        ---int                                 
		,@INDUSOOBJ                                  ---NUMERIC(15,2)                                 
		,@SUBSERIE                                   ---varchar(8)                                 
		,@STSCOMPRAS                                 ---TINYINT                                 
		,@CODLOCEXP                                  ---VARCHAR(15)                                 
		,@IDCLASSMOV                                 ---INT                                 
		,@CODENTREGA                                 ---VARCHAR(15)                                 
		,@CODFAIXAENTREGA                            ---VARCHAR(15)                                 
		,@DTHENTREGA                                 ---DATETIME                                 
		,@CONTABILIZADOPORTOTAL                      ---SMALLINT                                 
		,@CODLAFE                                    ---varchar(15)                                 
		,@IDPRJ                                      ---int                                 
		,@NUMEROCUPOM                                ---int                                 
		,@NUMEROCAIXA                                ---int                                 
		,@FLAGEFEITOSALDO                            ---smallint                                 
		,@INTEGRADOBONUM                             ---SMALLINT                                 
		,@CODMOELANCAMENTO                           ---varchar(10)                                 
		,@NAONUMERADO                                ---varchar(1)                                 
		,@FLAGPROCESSADO                             ---SMALLINT                                 
		,@ABATIMENTOICMS                             ---NUMERIC(15,2)                                 
		,@TIPOCONSUMO                                ---smallint                                 
		,@HORARIOEMISSAO                             ---datetime                                 
		,@DATARETORNO                                ---datetime                                 
		,@USUARIOCRIACAO                             ---varchar(20)                                 
		,@DATACRIACAO                                ---datetime                                 
		,@IDCONTATOENTREGA                           ---int                                 
		,@IDCONTATOCOBRANCA                          ---int                                 
		,@STATUSSEPARACAO                            ---varchar(1)                                 
		,@STSEMAIL                                   ---SMALLINT                                 
		,@VALORFRETECTRC                             ---NUMERIC(15,2)                                 
		,@PONTOVENDA                                 ---varchar(10)                                 
		,@PRAZOENTREGA                               ---int                                 
		,@VALORBRUTOINTERNO                          ---NUMERIC(15,2)                                 
		,@IDAIDF                                     ---smallint                                 
		,@IDSALDOESTOQUE                             ---int                                 
		,@VINCULADOESTOQUEFL                         ---SMALLINT   
		,@IDREDUCAOZ                                 ---int                                 
		,@HORASAIDA                                  ---datetime                                 
		,@CODMUNSERVICO                              ---varchar(20)                                 
		,@CODETDMUNSERV                              ---varchar(2)                                 
		,@APROPRIADO                                 ---smallint                                 
		,@CODIGOSERVICO                              ---varchar(15)                                 
		,@DATADEDUCAO                                ---datetime                                 
		,@CODDIARIO                                  ---varchar(5)                                 
		,@SEQDIARIO                                  ---varchar(9)                                 
		,@SEQDIARIOESTORNO                           ---varchar(9)                                 
		,@INSSEMOUTRAEMPRESA                         ---NUMERIC(15,2)                                 
		,@IDMOVCTRC                                  ---int                                 
		,@DATAPROGRAMACAOANT                         ---datetime                                 
		,@CODTDO                                     ---varchar(10)                                 
		,@VALORDESCCONDICIONAL                       ---NUMERIC(15,2)                                 
		,@VALORDESPCONDICIONAL                       ---NUMERIC(15,2)                                 
		,@CODIGOIRRF                                 ---varchar(10)                                 
		,@DEDUCAOIRRF                                ---NUMERIC(15,2)                                 
		,@PERCENTBASEINSS                            ---NUMERIC(15,2)                                 
		,@PERCBASEINSSEMPREGADO                      ---NUMERIC(15,2)                                 
		,@CONTORCAMENTOANTIGO                        ---SMALLINT                                 
		,@CODDEPTODESTINO                            ---varchar(25)                                 
		,@DATACONTABILIZACAO                         ---datetime                                 
		,@CODVIATRANSPORTE                           ---varchar(1)                                 
		,@VALORSERVICO                               ---NUMERIC(15,2)                                 
		,@SEQUENCIALESTOQUE                          ---int                                 
		,@DISTANCIA                                  ---int                                 
		,@UNCALCULO                                  ---varchar(1)                                 
		,@FORMACALCULO                               ---varchar(1)                                 
		,@INTEGRADOAUTOMACAO                         ---smallint                                 
		,@INTEGRAAPLICACAO                           ---char(1)                                 
		,@CLASSECONSUMO                              ---varchar(1)                                 
		,@TIPOASSINANTE                              ---varchar(1)                                 
		,@FASE                                       ---varchar(1)                                 
		,@TIPOUTILIZACAO                             ---varchar(1)                                 
		,@GRUPOTENSAO                                ---varchar(1)                                 
		,@DATALANCAMENTO                             ---datetime                                 
		,@EXTENPORANEO                               ---SMALLINT                                 
		,@RECIBONFESTATUS                            ---varchar(1)                                 
		,@RECIBONFETIPO                              ---smallint                                 
		,@RECIBONFENUMERO                            ---varchar(12)                                 
		,@RECIBONFESITUACAO                          ---smallint                                 
		,@IDMOVCFO                ---int                                 
		,@OCAUTONOMO                                 ---smallint                                 
		,@VALORMERCADORIAS                           ---NUMERIC(15,2)                                 
		,@NATUREZAVOLUMES                            ---varchar(30)                                 
		,@VOLUMES                                    ---varchar(30)                                 
		,@CRO                                        ---smallint                                 
		,@USARATEIOVALORFIN                          ---SMALLINT                                 
		,@RECIBONFESERIE                             ---varchar(5)                                 
		,@CODCOLCFOORIGEM                            ---SMALLINT                                 
		,@CODCFOORIGEM                               ---varchar(25)                                 
		,@VALORCTRCARATEAR                           ---NUMERIC(15,2)                                 
		,@CODCOLCFOAUX                               ---smallint                                 
		,@VRBASEINSSOUTRAEMPRESA                     ---NUMERIC(15,2)                                 
		,@IDCEICFO                                   ---int                                 
		,@CHAVEACESSONFE                             ---varchar(44)                                 
		,@VLRSECCAT                                  ---NUMERIC(15,2)                                 
		,@VLRDESPACHO                                ---NUMERIC(15,2)                                 
		,@VLRPEDAGIO                                 ---NUMERIC(15,2)                                 
		,@VLRFRETEOUTROS                             ---NUMERIC(15,2)                                 
		,@ABATIMENTONAOTRIB                          ---NUMERIC(15,2)                                 
		,@RATEIOCCUSTODEPTO                          ---NUMERIC(15,2)                                 
		,@VALORRATEIOLAN                             ---NUMERIC(15,2)                                 
		,@CODCOLCFOTRANSFAT                          ---SMALLINT                                 
		,@CODCFOTRANSFAT                             ---varchar(25)                                 
		,@CODUSUARIOAPROVADESC                       ---varchar(20)
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL   
	);
           
	/*ATUALIZA GAUTOINC - IDMOV*/
	UPDATE CORPORERMATT..GAUTOINC
	SET VALAUTOINC    = (SELECT VALAUTOINC+1 FROM CORPORERMATT..GAUTOINC
						 WHERE CODAUTOINC = 'IDMOV'
						 AND CODCOLIGADA = @CODCOLIGADA)
	WHERE CODAUTOINC  = 'IDMOV'
	AND   CODCOLIGADA = @CODCOLIGADA;
                     		  
END  /****** F I M -> BLOCO INSERT (ENTRADAS) *****/            		  
                     		  

                     		  
/*        I N S E R T         D E         M O V I M E N T O         ( S A Í D A S )*/ 
-------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT CODCFO FROM CORPORERMATT..TMOV
			   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
			   AND CODCOLIGADA = @CODCOLIGADA )                         
AND @TIPO_MOV = 'S'
                 
BEGIN

	-- JAILSON (11/07/2011)
	IF NOT EXISTS (SELECT VALAUTOINC FROM CORPORERMATT..GAUTOINC
						 WHERE CODAUTOINC = 'IDMOV' 
						 AND CODCOLIGADA=@CODCOLIGADA)
	BEGIN
		INSERT INTO CORPORERMATT..GAUTOINC
		(CODCOLIGADA,CODSISTEMA,CODAUTOINC,VALAUTOINC)
		VALUES
		(@CODCOLIGADA,'T','IDMOV',0)
	END

	DECLARE @IDMOV_IS   INT;
	SELECT  @IDMOV_IS   = (SELECT VALAUTOINC+1 FROM CORPORERMATT..GAUTOINC WHERE CODAUTOINC = 'IDMOV' 
	   AND CODCOLIGADA=@CODCOLIGADA);

	INSERT INTO [CORPORERMATT].[dbo].[TMOV]
		  ([CODCOLIGADA]
		 ,[IDMOV]
		 ,[CODFILIAL]
		 ,[CODLOC]
		 ,[CODLOCENTREGA]
		 ,[CODLOCDESTINO]
		 ,[CODCFO]
		 ,[CODCFONATUREZA]
		 ,[NUMEROMOV]
		 ,[SERIE]
		 ,[CODTMV]
		 ,[TIPO]
		 ,[STATUS]
		 ,[MOVIMPRESSO]
		 ,[DOCIMPRESSO]
		 ,[FATIMPRESSA]
		 ,[DATAEMISSAO]
		 ,[DATASAIDA]
		 ,[DATAEXTRA1]
		 ,[DATAEXTRA2]
		 ,[CODRPR]
		 ,[COMISSAOREPRES]
		 ,[NORDEM]
		 ,[CODCPG]
		 ,[NUMEROTRIBUTOS]
		 ,[VALORBRUTO]
		 ,[VALORLIQUIDO]
		 ,[VALOROUTROS]
		 ,[OBSERVACAO]
		 ,[PERCENTUALFRETE]
		 ,[VALORFRETE]
		 ,[PERCENTUALSEGURO]
		 ,[VALORSEGURO]
		 ,[PERCENTUALDESC]
		 ,[VALORDESC]
		 ,[PERCENTUALDESP]
		 ,[VALORDESP]
		 ,[PERCENTUALEXTRA1]
		 ,[VALOREXTRA1]
		 ,[PERCENTUALEXTRA2]
		 ,[VALOREXTRA2]
		 ,[PERCCOMISSAO]
		 ,[CODMEN]
		 ,[CODMEN2]
		 ,[VIADETRANSPORTE]
		 ,[PLACA]
		 ,[CODETDPLACA]
		 ,[PESOLIQUIDO]
		 ,[PESOBRUTO]
		 ,[MARCA]
		 ,[NUMERO]
		 ,[QUANTIDADE]
		 ,[ESPECIE]
		 ,[CODTB1FAT]
		 ,[CODTB2FAT]
		 ,[CODTB3FAT]
		 ,[CODTB4FAT]
		 ,[CODTB5FAT]
		 ,[CODTB1FLX]
		 ,[CODTB2FLX]
		 ,[CODTB3FLX]
		 ,[CODTB4FLX]
		 ,[CODTB5FLX]
		 ,[IDMOVRELAC]
		 ,[IDMOVLCTFLUXUS]
		 ,[IDMOVPEDDESDOBRADO]
		 ,[CODMOEVALORLIQUIDO]
		 ,[DATABASEMOV]
		 ,[DATAMOVIMENTO]
		 ,[NUMEROLCTGERADO]
		 ,[GEROUFATURA]
		 ,[NUMEROLCTABERTO]
		 ,[FLAGEXPORTACAO]
		 ,[EMITEBOLETA]
		 ,[CODMENDESCONTO]
		 ,[CODMENDESPESA]
		 ,[CODMENFRETE]
		 ,[FRETECIFOUFOB]
		 ,[USADESPFINANC]
		 ,[FLAGEXPORFISC]
		 ,[FLAGEXPORFAZENDA]
		 ,[VALORADIANTAMENTO]
		 ,[CODTRA]
		 ,[CODTRA2]
		 ,[STATUSLIBERACAO]
		 ,[CODCFOAUX]
		 ,[IDLOT]
		 ,[ITENSAGRUPADOS]
		 ,[FLAGIMPRESSAOFAT]
		 ,[DATACANCELAMENTOMOV]
		 ,[VALORRECEBIDO]
		 ,[SEGUNDONUMERO]
		 ,[CODCCUSTO]
		 ,[CODCXA]
		 ,[CODVEN1]
		 ,[CODVEN2]
		 ,[CODVEN3]
		 ,[CODVEN4]
		 ,[PERCCOMISSAOVEN2]
		 ,[CODCOLCFO]
		 ,[CODCOLCFONATUREZA]
		 ,[CODUSUARIO]
		 ,[CODFILIALENTREGA]
		 ,[CODFILIALDESTINO]
		 ,[FLAGAGRUPADOFLUXUS]
		 ,[CODCOLCXA]
		 ,[GERADOPORLOTE]
		 ,[CODDEPARTAMENTO]
		 ,[CODCCUSTODESTINO]
		 ,[CODEVENTO]
		 ,[STATUSEXPORTCONT]
		 ,[CODLOTE]
		 ,[STATUSCHEQUE]
		 ,[DATAENTREGA]
		 ,[DATAPROGRAMACAO]
		 ,[IDNAT]
		 ,[IDNAT2]
		 ,[CAMPOLIVRE1]
		 ,[CAMPOLIVRE2]
		 ,[CAMPOLIVRE3]
		 ,[GEROUCONTATRABALHO]
		 ,[GERADOPORCONTATRABALHO]
		 ,[HORULTIMAALTERACAO]
		 ,[CODLAF]
		 ,[DATAFECHAMENTO]
		 ,[NSEQDATAFECHAMENTO]
		 ,[NUMERORECIBO]
		 ,[IDLOTEPROCESSO]
		 ,[IDOBJOF]
		 ,[CODAGENDAMENTO]
		 ,[CHAPARESP]
		 ,[IDLOTEPROCESSOREFAT]
		 ,[INDUSOOBJ]
		 ,[SUBSERIE]
		 ,[STSCOMPRAS]
		 ,[CODLOCEXP]
		 ,[IDCLASSMOV]
		 ,[CODENTREGA]
		 ,[CODFAIXAENTREGA]
		 ,[DTHENTREGA]
		 ,[CONTABILIZADOPORTOTAL]
		 ,[CODLAFE]
		 ,[IDPRJ]
		 ,[NUMEROCUPOM]
		 ,[NUMEROCAIXA]
		 ,[FLAGEFEITOSALDO]
		 ,[INTEGRADOBONUM]
		 ,[CODMOELANCAMENTO]
		 ,[NAONUMERADO]
		 ,[FLAGPROCESSADO]
		 ,[ABATIMENTOICMS]
		 ,[TIPOCONSUMO]
		 ,[HORARIOEMISSAO]
		 ,[DATARETORNO]
		 ,[USUARIOCRIACAO]
		 ,[DATACRIACAO]
		 ,[IDCONTATOENTREGA]
		 ,[IDCONTATOCOBRANCA]
		 ,[STATUSSEPARACAO]
		 ,[STSEMAIL]
		 ,[VALORFRETECTRC]
		 ,[PONTOVENDA]
		 ,[PRAZOENTREGA]
		 ,[VALORBRUTOINTERNO]
		 ,[IDAIDF]
		 ,[IDSALDOESTOQUE]
		 ,[VINCULADOESTOQUEFL]
		 ,[IDREDUCAOZ]
		 ,[HORASAIDA]
		 ,[CODMUNSERVICO]
		 ,[CODETDMUNSERV]
		 ,[APROPRIADO]
		 ,[CODIGOSERVICO]
		 ,[DATADEDUCAO]
		 ,[CODDIARIO]
		 ,[SEQDIARIO]
		 ,[SEQDIARIOESTORNO]
		 ,[INSSEMOUTRAEMPRESA]
		 ,[IDMOVCTRC]
		 ,[DATAPROGRAMACAOANT]
		 ,[CODTDO]
		 ,[VALORDESCCONDICIONAL]
		 ,[VALORDESPCONDICIONAL]
		 ,[CODIGOIRRF]
		 ,[DEDUCAOIRRF]
		 ,[PERCENTBASEINSS]
		 ,[PERCBASEINSSEMPREGADO]
		 ,[CONTORCAMENTOANTIGO]
		 ,[CODDEPTODESTINO]
		 ,[DATACONTABILIZACAO]
		 ,[CODVIATRANSPORTE]
		 ,[VALORSERVICO]
		 ,[SEQUENCIALESTOQUE]
		 ,[DISTANCIA]
		 ,[UNCALCULO]
		 ,[FORMACALCULO]
		 ,[INTEGRADOAUTOMACAO]
		 ,[INTEGRAAPLICACAO]
		 ,[CLASSECONSUMO]
		 ,[TIPOASSINANTE]
		 ,[FASE]
		 ,[TIPOUTILIZACAO]
		 ,[GRUPOTENSAO]
		 ,[DATALANCAMENTO]
		 ,[EXTENPORANEO]
		 ,[RECIBONFESTATUS]
		 ,[RECIBONFETIPO]
		 ,[RECIBONFENUMERO]
		 ,[RECIBONFESITUACAO]
		 ,[IDMOVCFO]
		 ,[OCAUTONOMO]
		 ,[VALORMERCADORIAS]
		 ,[NATUREZAVOLUMES]
		 ,[VOLUMES]
		 ,[CRO]
		 ,[USARATEIOVALORFIN]
		 ,[RECIBONFESERIE]
		 ,[CODCOLCFOORIGEM]
		 ,[CODCFOORIGEM]
		 ,[VALORCTRCARATEAR]
		 ,[CODCOLCFOAUX]
		 ,[VRBASEINSSOUTRAEMPRESA]
		 ,[IDCEICFO]
		 ,[CHAVEACESSONFE]
		 ,[VLRSECCAT]
		 ,[VLRDESPACHO]
		 ,[VLRPEDAGIO]
		 ,[VLRFRETEOUTROS]
		 ,[ABATIMENTONAOTRIB]
		 ,[RATEIOCCUSTODEPTO]
		 ,[VALORRATEIOLAN]
		 ,[CODCOLCFOTRANSFAT]
		 ,[CODCFOTRANSFAT]
		 ,[CODUSUARIOAPROVADESC]
		 ,[IDINTEGRACAO]
		 ,[STATUSANTERIOR]
		 ,[VALORBRUTOORIG]
		 ,[VALORLIQUIDOORIG]
		 ,[VALOROUTROSORIG]
		 ,[VALORRATEIOLANORIG]
		 ,[DATAPROCESSAMENTO]
		 ,[IDNATFRETE]
		 ,[IDOPERACAO]
		 ,[RECCREATEDBY]
		 ,[RECCREATEDON]
		 ,[RECMODIFIEDBY]
		 ,[RECMODIFIEDON])
		 
	VALUES
		(@CODCOLIGADA                              ---SMALLINT      
		,@IDMOV_IS                                 ---int  IDMOV                                 
		,@CODFILIAL                                ---smallint                                 
		,@CODLOC                                     ---varchar(15)                                 
		,@CODLOCENTREGA                              ---varchar(15)                                 
		,@CODLOCDESTINO                              ---varchar(15)                                 
		,@CODCFO                                     ---varchar(25)                                 
		,@CODCFONATUREZA                             ---varchar(25)                                 
		,@NUMEROMOV                                  ---varchar(35)                                 
		,@SERIE                                      ---varchar(8)                                 
		,@CODTMV                                     ---varchar(10)                                 
		,@TIPO                                       ---varchar(1)                                 
		,@STATUS                                     ---varchar(1)                                 
		,@MOVIMPRESSO                                ---smallint                                 
		,@DOCIMPRESSO                                ---smallint                                 
		,@FATIMPRESSA                                ---smallint                                 
		,@DATAEMISSAO                                ---datetime                                 
		,@DATASAIDA                                  ---datetime                                 
		,@DATAEXTRA1                                 ---datetime                                 
		,@DATAEXTRA2                                 ---datetime                                 
		,@CODRPR                                     ---varchar(15)                                 
		,@COMISSAOREPRES                             ---NUMERIC(15,2)                                 
		,@NORDEM                                     ---varchar(20)                                 
		,@CODCPG                                     ---varchar(5)                                 
		,@NUMEROTRIBUTOS                             ---smallint                                 
		,@VALORBRUTO                                 ---NUMERIC(15,2)                                 
		,@VALORLIQUIDO                               ---NUMERIC(15,2)                                 
		,@VALOROUTROS                                ---NUMERIC(15,2)                                 
		,@OBSERVACAO                                 ---varchar(60)                                 
		,@PERCENTUALFRETE                            ---NUMERIC(15,2)                                 
		,@VALORFRETE                                 ---NUMERIC(15,2)                                 
		,@PERCENTUALSEGURO                           ---NUMERIC(15,2)                                 
		,@VALORSEGURO                                ---NUMERIC(15,2)                                 
		,@PERCENTUALDESC                             ---NUMERIC(15,2)                                 
		,@VALORDESC                ---NUMERIC(15,2)                                 
		,@PERCENTUALDESP                             ---NUMERIC(15,2)                                 
		,@VALORDESP                                  ---NUMERIC(15,2)                                 
		,@PERCENTUALEXTRA1                           ---NUMERIC(15,2)                                 
		,@VALOREXTRA1                                ---NUMERIC(15,2)                                 
		,@PERCENTUALEXTRA2                           ---NUMERIC(15,2)                                 
		,@VALOREXTRA2                                ---NUMERIC(15,2)                                 
		,@PERCCOMISSAO                               ---NUMERIC(15,2)                                 
		,@CODMEN                                     ---varchar(5)                                 
		,@CODMEN2                                    ---varchar(5)                                 
		,@VIADETRANSPORTE                            ---varchar(15)                                 
		,@PLACA                                      ---varchar(10)                                 
		,@CODETDPLACA                                ---varchar(2)                                 
		,@PESOLIQUIDO                                ---NUMERIC(15,2)                                 
		,@PESOBRUTO                                  ---NUMERIC(15,2)                                 
		,@MARCA                                      ---varchar(10)                                 
		,@NUMERO                                     ---int                                 
		,@QUANTIDADE                                 ---int                                 
		,@ESPECIE                                    ---varchar(15)                                 
		,@CODTB1FAT                                  ---varchar(10)                                 
		,@CODTB2FAT                                  ---varchar(10)                                 
		,@CODTB3FAT                                  ---varchar(10)                                 
		,@CODTB4FAT                                  ---varchar(10)                                 
		,@CODTB5FAT                                  ---varchar(10)                                 
		,@CODTB1FLX                                  ---varchar(25)                                 
		,@CODTB2FLX                                  ---varchar(25)                                 
		,@CODTB3FLX                                  ---varchar(25)                                 
		,@CODTB4FLX                                  ---varchar(25)                                 
		,@CODTB5FLX                                  ---varchar(25)                                 
		,@IDMOVRELAC                                 ---int                                 
		,@IDMOVLCTFLUXUS                             ---int                                 
		,@IDMOVPEDDESDOBRADO                         ---int                                 
		,@CODMOEVALORLIQUIDO                         ---varchar(10)                                 
		,@DATABASEMOV                                ---datetime                                 
		,@DATAMOVIMENTO                              ---datetime                                 
		,@NUMEROLCTGERADO                            ---smallint                                 
		,@GEROUFATURA                                ---smallint                                 
		,@NUMEROLCTABERTO                            ---smallint                                 
		,@FLAGEXPORTACAO                             ---smallint                                 
		,@EMITEBOLETA                                ---varchar(1)                                 
		,@CODMENDESCONTO                             ---varchar(5)                                 
		,@CODMENDESPESA                              ---varchar(5)                                 
		,@CODMENFRETE    ---varchar(5)                                 
		,@FRETECIFOUFOB                              ---smallint                                 
		,@USADESPFINANC                              ---smallint                                 
		,@FLAGEXPORFISC                              ---smallint                                 
		,@FLAGEXPORFAZENDA                           ---smallint                                 
		,@VALORADIANTAMENTO                          ---NUMERIC(15,2)                                 
		,@CODTRA                                     ---varchar(5)                                 
		,@CODTRA2                                    ---varchar(5)                                 
		,@STATUSLIBERACAO                            ---smallint                                 
		,@CODCFOAUX                                  ---varchar(25)                                 
		,@IDLOT                                      ---int                                 
		,@ITENSAGRUPADOS                             ---smallint                                 
		,@FLAGIMPRESSAOFAT                           ---varchar(1)                                 
		,@DATACANCELAMENTOMOV                        ---datetime                                 
		,@VALORRECEBIDO                              ---NUMERIC(15,2)                                 
		,@SEGUNDONUMERO                              ---varchar(20)                                 
		,@CODCCUSTO                                  ---varchar(25)                                 
		,@CODCXA                                     ---varchar(10)                                 
		,@CODVEN1                                    ---varchar(16)                                 
		,@CODVEN2                                    ---varchar(16)                                 
		,@CODVEN3                                    ---varchar(16)                                 
		,@CODVEN4                                    ---varchar(16)                                 
		,@PERCCOMISSAOVEN2                           ---NUMERIC(15,2)                                 
		,@CODCOLCFO                                  ---smallint                                 
		,@CODCOLCFONATUREZA                          ---smallint                                 
		,@CODUSUARIO                                 ---varchar(20)                                 
		,@CODFILIALENTREGA                           ---smallint                                 
		,@CODFILIALDESTINO                           ---smallint                                 
		,@FLAGAGRUPADOFLUXUS                         ---smallint                                 
		,@CODCOLCXA                                  ---SMALLINT                                 
		,@GERADOPORLOTE                              ---smallint                                 
		,@CODDEPARTAMENTO                            ---varchar(25)                                 
		,@CODCCUSTODESTINO                           ---varchar(25)                                 
		,@CODEVENTO                                  ---smallint                                 
		,@STATUSEXPORTCONT                           ---smallint                                 
		,@CODLOTE                                    ---int                                 
		,@STATUSCHEQUE                               ---smallint                                 
		,@DATAENTREGA                                ---datetime                                 
		,@DATAPROGRAMACAO                            ---datetime                                 
		,@IDNAT                                      ---int                                 
		,@IDNAT2                                     ---int                                 
		,@CAMPOLIVRE1                                ---varchar(100)                                 
		,@CAMPOLIVRE2                                ---varchar(100)                                 
		,@CAMPOLIVRE3                                ---varchar(100)                                 
		,@GEROUCONTATRABALHO                         ---SMALLINT                                 
		,@GERADOPORCONTATRABALHO                     ---SMALLINT                                 
		,@HORULTIMAALTERACAO                         ---datetime                                 
		,@CODLAF                                     ---varchar(15)                                 
		,@DATAFECHAMENTO                             ---datetime                                 
		,@NSEQDATAFECHAMENTO                         ---smallint                                 
		,@NUMERORECIBO                               ---varchar(12)                                 
		,@IDLOTEPROCESSO                             ---int                                 
		,@IDOBJOF                                    ---varchar(20)                                 
		,@CODAGENDAMENTO                             ---int                                 
		,@CHAPARESP                                  ---VARCHAR(16)                                 
		,@IDLOTEPROCESSOREFAT                        ---int                                 
		,@INDUSOOBJ                                  ---NUMERIC(15,2)                                 
		,@SUBSERIE                                   ---varchar(8)                                 
		,@STSCOMPRAS                                 ---TINYINT                                 
		,@CODLOCEXP                                  ---VARCHAR(15)                                 
		,@IDCLASSMOV                                 ---INT                                 
		,@CODENTREGA                                 ---VARCHAR(15)                                 
		,@CODFAIXAENTREGA                            ---VARCHAR(15)                                 
		,@DTHENTREGA                                 ---DATETIME                                 
		,@CONTABILIZADOPORTOTAL                      ---SMALLINT                                 
		,@CODLAFE                                    ---varchar(15)                                 
		,@IDPRJ                                      ---int                                 
		,@NUMEROCUPOM                                ---int                                 
		,@NUMEROCAIXA                                ---int                                 
		,@FLAGEFEITOSALDO                            ---smallint                                 
		,@INTEGRADOBONUM                             ---SMALLINT                                 
		,@CODMOELANCAMENTO                           ---varchar(10)                                 
		,@NAONUMERADO                                ---varchar(1)                                 
		,@FLAGPROCESSADO                             ---SMALLINT                                 
		,@ABATIMENTOICMS                             ---NUMERIC(15,2)                                 
		,@TIPOCONSUMO                                ---smallint                                 
		,@HORARIOEMISSAO                             ---datetime                                 
		,@DATARETORNO                                ---datetime                                 
		,@USUARIOCRIACAO                             ---varchar(20)                                 
		,@DATACRIACAO                                ---datetime                                 
		,@IDCONTATOENTREGA                           ---int                                 
		,@IDCONTATOCOBRANCA                          ---int                                 
		,@STATUSSEPARACAO                            ---varchar(1)                                 
		,@STSEMAIL                                   ---SMALLINT                                 
		,@VALORFRETECTRC                             ---NUMERIC(15,2)                                 
		,@PONTOVENDA                                 ---varchar(10)         
		,@PRAZOENTREGA                               ---int                                 
		,@VALORBRUTOINTERNO                          ---NUMERIC(15,2)                                 
		,@IDAIDF                                     ---smallint                                 
		,@IDSALDOESTOQUE                             ---int                                 
		,@VINCULADOESTOQUEFL                         ---SMALLINT                                 
		,@IDREDUCAOZ                                 ---int                                 
		,@HORASAIDA                                  ---datetime                                 
		,@CODMUNSERVICO                              ---varchar(20)                                 
		,@CODETDMUNSERV                              ---varchar(2)                                 
		,@APROPRIADO                                 ---smallint                                 
		,@CODIGOSERVICO                              ---varchar(15)                                 
		,@DATADEDUCAO                                ---datetime                                 
		,@CODDIARIO                                  ---varchar(5)                                 
		,@SEQDIARIO                                  ---varchar(9)                                 
		,@SEQDIARIOESTORNO                           ---varchar(9)                                 
		,@INSSEMOUTRAEMPRESA                         ---NUMERIC(15,2)                                 
		,@IDMOVCTRC                                  ---int                                 
		,@DATAPROGRAMACAOANT                         ---datetime                                 
		,@CODTDO                                     ---varchar(10)                                 
		,@VALORDESCCONDICIONAL                       ---NUMERIC(15,2)                                 
		,@VALORDESPCONDICIONAL                       ---NUMERIC(15,2)                                 
		,@CODIGOIRRF                                 ---varchar(10)                                 
		,@DEDUCAOIRRF                                ---NUMERIC(15,2)                                 
		,@PERCENTBASEINSS                            ---NUMERIC(15,2)                                 
		,@PERCBASEINSSEMPREGADO                      ---NUMERIC(15,2)                                 
		,@CONTORCAMENTOANTIGO                        ---SMALLINT                                 
		,@CODDEPTODESTINO                            ---varchar(25)                                 
		,@DATACONTABILIZACAO                         ---datetime                                 
		,@CODVIATRANSPORTE                           ---varchar(1)                                 
		,@VALORSERVICO                               ---NUMERIC(15,2)                                 
		,@SEQUENCIALESTOQUE                          ---int                                 
		,@DISTANCIA                                  ---int                                 
		,@UNCALCULO                                  ---varchar(1)                                 
		,@FORMACALCULO                               ---varchar(1)                                 
		,@INTEGRADOAUTOMACAO                         ---smallint                                 
		,@INTEGRAAPLICACAO                           ---char(1)                                 
		,@CLASSECONSUMO                              ---varchar(1)                                 
		,@TIPOASSINANTE                              ---varchar(1)                                 
		,@FASE                                       ---varchar(1)                                 
		,@TIPOUTILIZACAO                             ---varchar(1)                                 
		,@GRUPOTENSAO                                ---varchar(1)                                 
		,@DATALANCAMENTO                             ---datetime                                 
		,@EXTENPORANEO            ---SMALLINT                                 
		,@RECIBONFESTATUS                            ---varchar(1)                                 
		,@RECIBONFETIPO                              ---smallint                                 
		,@RECIBONFENUMERO                            ---varchar(12)                                 
		,@RECIBONFESITUACAO                          ---smallint                                 
		,@IDMOVCFO                                   ---int                                 
		,@OCAUTONOMO                                 ---smallint                                 
		,@VALORMERCADORIAS                           ---NUMERIC(15,2)                                 
		,@NATUREZAVOLUMES                            ---varchar(30)                                 
		,@VOLUMES                                    ---varchar(30)                                 
		,@CRO                                        ---smallint                                 
		,@USARATEIOVALORFIN                          ---SMALLINT                                 
		,@RECIBONFESERIE                             ---varchar(5)                                 
		,@CODCOLCFOORIGEM                            ---SMALLINT                                 
		,@CODCFOORIGEM                               ---varchar(25)                                 
		,@VALORCTRCARATEAR                           ---NUMERIC(15,2)                                 
		,@CODCOLCFOAUX                               ---smallint                                 
		,@VRBASEINSSOUTRAEMPRESA                     ---NUMERIC(15,2)                                 
		,@IDCEICFO                                   ---int                                 
		,@CHAVEACESSONFE                             ---varchar(44)                                 
		,@VLRSECCAT                                  ---NUMERIC(15,2)                                 
		,@VLRDESPACHO                                ---NUMERIC(15,2)                                 
		,@VLRPEDAGIO                                 ---NUMERIC(15,2)                                 
		,@VLRFRETEOUTROS                             ---NUMERIC(15,2)                                 
		,@ABATIMENTONAOTRIB                          ---NUMERIC(15,2)                                 
		,@RATEIOCCUSTODEPTO                          ---NUMERIC(15,2)                                 
		,@VALORRATEIOLAN                             ---NUMERIC(15,2)                                 
		,@CODCOLCFOTRANSFAT                          ---SMALLINT                                 
		,@CODCFOTRANSFAT                             ---varchar(25)                                 
		,@CODUSUARIOAPROVADESC                       ---varchar(20)  
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL
		,NULL);
           
	/*ATUALIZA GAUTOINC - IDMOV*/
	UPDATE CORPORERMATT..GAUTOINC
	SET VALAUTOINC = (SELECT VALAUTOINC+1 FROM CORPORERMATT..GAUTOINC
					  WHERE CODAUTOINC = 'IDMOV'
					  AND CODCOLIGADA = @CODCOLIGADA)
	WHERE CODAUTOINC  = 'IDMOV'
	AND   CODCOLIGADA = @CODCOLIGADA;

END  /******   F I M  ->  BLOCO  INSERT  (SAÍDAS)  *****/



/*   T R I B U T A C A O    E    C A M P O   C O M P L E M E N T A R   D O     M O V I M E N T O   ( E N T R A D A S )*/ 
------------------------------------------------------------------------------------------------------------------------
/*INICIO BLOCO INSERT DE TRIBUTAÇÃO DO MOVIMENTO E CAMPO COMPLEMENTAR (ENTRADAS)*/

/* JAILSON (26/06/2012) - PIS E COFINS SERÁ UTILIZADO POR ITEM 

IF @TIPO_MOV = 'E'
        BEGIN
        
              /* I N S E R T      T R I B U T A Ç Ã O      D O      M O V I M E N T O   -   ENTRADAS*/
              SET @IDMOV_IE = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
							   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
							   AND CODCOLIGADA = @CODCOLIGADA)
              --DECLARA VARIAVEIS
              DECLARE @VLRCOFINS_IE       NUMERIC(15,2)
                     ,@ALIQCOFINS_IE      NUMERIC(15,2)
                     ,@VLRBASECOFINS_IE   NUMERIC(15,2)
                     ,@VLRPIS_IE          NUMERIC(15,2)
                     ,@ALIQPIS_IE         NUMERIC(15,2)
                     ,@VLRBASEPIS_IE      NUMERIC(15,2) 
          
              --SETA VARIAREIS PIS E COFINS
		      SELECT  @VLRCOFINS_IE     = TOTALCOFINS
			    	 ,@ALIQCOFINS_IE    = ALIQUOTACOFINS
				     ,@VLRBASECOFINS_IE = BASECOFINS
				     ,@VLRPIS_IE        = TOTALPIS
				     ,@ALIQPIS_IE       = ALIQUOTAPIS
				     ,@VLRBASEPIS_IE    = BASEPIS
		      FROM INTEGRACAOTE..COMP_ENTRADA A
	          WHERE CONVERT(VARCHAR(25),A.COMPENTRADA) = @ID_MOVIMENTO          

             /* C O F I N S */
             SET @IDMOV_IE = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
							  WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
							  AND CODCOLIGADA = @CODCOLIGADA)   
             IF NOT EXISTS (SELECT * FROM CORPORERMATT..TTRBMOV
							WHERE IDMOV = @IDMOV_IE
							AND CODCOLIGADA = @CODCOLIGADA
							AND CODTRB = 'COFINS'
							AND NSEQITMMOV = 0)
                  BEGIN   
                        INSERT INTO CORPORERMATT..TTRBMOV (                      
									codcoligada,
									idmov,
									nseqitmmov,
									codtrb,
									basedecalculo,
									aliquota,
									valor,
									fatorreducao,
									fatorsubsttrib,
									basedecalculocalculada,
									editado,
									vlrisento,
									codretencao,
									tiporecolhimento,
									codtrbbase)
						 VALUES    (@CODCOLIGADA,
								    @IDMOV_IE,
							     	0,
								    'COFINS',
								    @VLRBASECOFINS_IE,
								    @ALIQCOFINS_IE,
								    @VLRCOFINS_IE,
								    0,
							  	    0,
								    @VLRBASECOFINS_IE,
								    1,
								    null,
								    null,
								    null,
								    null)                
                   END 

             /* P I S */  
             SET @IDMOV_IE = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
							  WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
							  AND CODCOLIGADA = @CODCOLIGADA)
             IF NOT EXISTS (SELECT * FROM CORPORERMATT..TTRBMOV
							WHERE IDMOV = @IDMOV_IE
							AND CODCOLIGADA = @CODCOLIGADA
							AND CODTRB = 'PIS'
							AND NSEQITMMOV = 0)
                   BEGIN   
                       INSERT INTO CORPORERMATT..TTRBMOV (                      
									codcoligada,
									idmov,
									nseqitmmov,
									codtrb,
									basedecalculo,
									aliquota,
									valor,
									fatorreducao,
									fatorsubsttrib,
									basedecalculocalculada,
									editado,
									vlrisento,
									codretencao,
									tiporecolhimento,
									codtrbbase)
						VALUES    (@CODCOLIGADA,
								   @IDMOV_IE,
								   0,
								   'PIS',
								   @VLRBASEPIS_IE,
								   @ALIQPIS_IE,
								   @VLRPIS_IE,
								   0,
								   0,
								   @VLRBASEPIS_IE,
							       1,
								   null,
								   null,
								   null,
								   null)                
          END 
      
          /* INICIO DE BLOCO DE INSERT DE CAMPO COMPLEMENTAR (ENTRADAS) */ 
          ----
          ----INSIRA O COD. DE INSERT DE CAMPO COMPLEMENTAR PARA ENTRADAS 
          ----NESSE ESPACO.
          ----
          /* FIM DO BLOCO DE INSERT DE CAMPO COMPLEMENTAR (ENTRADAS)    */
     
     
     
     
END/*<----------FIM BLOCO DE INSERT DE TRIBUTAÇÃO E CAMPO COMPLEMENTAR*/  
     
*/
     
     

/*   T R I B U T A C A O    E    C A M P O   C O M P L E M E N T A R   D O     M O V I M E N T O   ( S A Í D A S )*/ 
--------------------------------------------------------------------------------------------------------------------
/*INICIO BLOCO INSERT DE TRIBUTAÇÃO DO MOVIMENTO E CAMPO COMPLEMENTAR (SAÍDAS)*/
IF @TIPO_MOV = 'S'
	BEGIN
		
		/* JAILSON (26/06/2012) - PIS E COFINS SERÁ UTILIZADO POR ITEM 
		
		
		/* I N S E R T      T R I B U T A Ç Ã O      D O      M O V I M E N T O   -   SAIDAS*/
		SET @IDMOV_IS = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						 WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						 AND CODCOLIGADA = @CODCOLIGADA)

		--DECLARA VARIAVEIS
		DECLARE @VLRCOFINS_IS	  NUMERIC(15,2)
			 ,@ALIQCOFINS_IS      NUMERIC(15,2)
			 ,@VLRBASECOFINS_IS   NUMERIC(15,2)
			 ,@VLRPIS_IS          NUMERIC(15,2)
			 ,@ALIQPIS_IS         NUMERIC(15,2)
			 ,@VLRBASEPIS_IS      NUMERIC(15,2) 
			 -- JAILSON NFE (30/03/2009)
			 ,@v_cstpiscof		  VARCHAR(2)
			 
		--SETA VARIAREIS PIS E COFINS
		SELECT @VLRCOFINS_IS	= TOTALCOFINS
			 ,@ALIQCOFINS_IS    = ALIQUOTACOFINS
			 ,@VLRBASECOFINS_IS = TotalNotaFiscal
			 ,@VLRPIS_IS        = TOTALPIS
			 ,@ALIQPIS_IS       = ALIQUOTAPIS
			 ,@VLRBASEPIS_IS    = TotalNotaFiscal
		FROM INTEGRACAOTE..FATURA_NOTA_FISCAL A
		WHERE CONVERT(VARCHAR(25),A.FATURANOTAFISCAL) = @ID_MOVIMENTO

		-- JAILSON NFE (07/04/2009)
		SET @v_cstpiscof='01'

		/*** JAILSON (21/06/2012)
		IF @ALIQPIS_IS=0
			SET @v_cstpiscof='07' ***/
			
		IF @ALIQPIS_IS=0
			SET @v_cstpiscof='08'
		
		-- 6.110.01
		IF @ALIQPIS_IS=0 AND @IDNAT=201
			SET @v_cstpiscof='07'
		
		/*** ***/
			
         /* C O F I N S */ 
        SET @IDMOV_IS = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						 WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						 AND CODCOLIGADA = @CODCOLIGADA)
						  
		IF NOT EXISTS (SELECT * FROM CORPORERMATT..TTRBMOV
					   WHERE IDMOV = @IDMOV_IS
					   AND CODCOLIGADA = @CODCOLIGADA
					   AND CODTRB = 'COFINS'
					   AND NSEQITMMOV = 0)
		BEGIN   
			
			INSERT INTO CORPORERMATT..TTRBMOV (                      
				codcoligada,
				idmov,
				nseqitmmov,
				codtrb,
				basedecalculo,
				aliquota,
				valor,
				fatorreducao,
				fatorsubsttrib,
				basedecalculocalculada,
				editado,
				vlrisento,
				codretencao,
				tiporecolhimento,
				codtrbbase,
				sittributaria)
			VALUES (
				@CODCOLIGADA,
				@IDMOV_IS,
				0,
				'COFINS',
				@VLRBASECOFINS_IS,
				@ALIQCOFINS_IS,
				@VLRCOFINS_IS,
				0,
				0,
				@VLRBASECOFINS_IS,
				1,
				null,
				null,
				null,
				null,
				@v_cstpiscof)                
		END 

			/* P I S */   
		SET @IDMOV_IS = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
					     WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
					     AND CODCOLIGADA = @CODCOLIGADA)
						   
		IF NOT EXISTS (SELECT * FROM CORPORERMATT..TTRBMOV
					   WHERE IDMOV = @IDMOV_IS
					   AND CODCOLIGADA = @CODCOLIGADA
					   AND CODTRB = 'PIS'
					   AND NSEQITMMOV = 0)
		BEGIN   
			INSERT INTO CORPORERMATT..TTRBMOV (                      
					codcoligada,
					idmov,
					nseqitmmov,
					codtrb,
					basedecalculo,
					aliquota,
					valor,
					fatorreducao,
					fatorsubsttrib,
					basedecalculocalculada,
					editado,
					vlrisento,
					codretencao,
					tiporecolhimento,
					codtrbbase,
					sittributaria)
					
			VALUES (@CODCOLIGADA,
					@IDMOV_IS,
					0,
					'PIS',
					@VLRBASEPIS_IS,
					@ALIQPIS_IS,
					@VLRPIS_IS,
					0,
					0,
					@VLRBASEPIS_IS,
					1,
					null,
					null,
					null,
					null,
					@v_cstpiscof)
		END */
		
		
        /*   CAMPO COMPLEMENTAR DO MOVIMENTO     */
        IF EXISTS (SELECT * FROM CORPORERMATT..TMOVCOMPL
				   WHERE IDMOV IN (SELECT IDMOV FROM CORPORERMATT..TMOV
								   WHERE CAMPOLIVRE1 = @ID_MOVIMENTO)
				   AND CODCOLIGADA = @CODCOLIGADA)
		BEGIN
			DELETE FROM CORPORERMATT..TMOVCOMPL
			WHERE  IDMOV IN (SELECT IDMOV FROM CORPORERMATT..TMOV
							 WHERE CAMPOLIVRE1 = @ID_MOVIMENTO)
			AND CODCOLIGADA = @CODCOLIGADA
			
		END;
        SET @IDMOV_IS = (SELECT TOP 1 IDMOV FROM CORPORERMATT..TMOV
						 WHERE CAMPOLIVRE1 = @ID_MOVIMENTO
						 AND CODCOLIGADA = @CODCOLIGADA)
		
		INSERT INTO CORPORERMATT..TMOVCOMPL
				(CODCOLIGADA
				,IDMOV
				,TRANSPORTE
				,DADOSADICIONAIS)
				
		SELECT  DISTINCT 
		        @CODCOLIGADA,
				@IDMOV_IS,
				transporte,
				dadosadicionais
				
		FROM INTEGRACAOTE..FATURA_NOTA_FISCAL
	    WHERE FATURANOTAFISCAL =@ID_MOVIMENTO;
	    
END /*FIM BLOCO DE INSERT DE TRIBUTAÇÃO E CAMPO COMPLEMENTAR (SAÍDAS)*/



/*   H I S T O R I C O       D O     M O V I M E N T O   */ 
----------------------------------------------------------
/*SINCRONISMO DO HISTÓRICO DA NOTA - ENTRADA / SAIDA*/
IF ( EXISTS (SELECT OBSERVACAODEVOLUCAO FROM INTEGRACAOTE..COMP_ENTRADA
			 WHERE COMPENTRADA = @ID_MOVIMENTO
			 AND OBSERVACAODEVOLUCAO IS NULL)
	OR EXISTS (SELECT DadosAdicionais FROM INTEGRACAOTE..FATURA_NOTA_FISCAL
			   WHERE FATURANOTAFISCAL = @ID_MOVIMENTO)
	)

BEGIN
	--PRINT 'HISTORICO'
            
	IF EXISTS (SELECT  H.IDMOV FROM CORPORERMATT..TMOVHISTORICO H, TMOV M 
			   WHERE   H.IDMOV        = M.IDMOV
			   AND     H.CODCOLIGADA  = M.CODCOLIGADA
			   AND     M.CAMPOLIVRE1  = @ID_MOVIMENTO)
	BEGIN
		
		UPDATE CORPORERMATT..TMOVHISTORICO
			                                  
		SET HISTORICOCURTO = CASE WHEN EXISTS (SELECT OBSERVACAODEVOLUCAO
											   FROM INTEGRACAOTE..COMP_ENTRADA
											   WHERE CONVERT(VARCHAR(25),COMPENTRADA) = @ID_MOVIMENTO
											   AND OBSERVACAODEVOLUCAO IS NULL)
								  THEN ISNULL((SELECT TOP 1 OBSERVACAODEVOLUCAO
											   FROM INTEGRACAOTE..COMP_ENTRADA
											   WHERE CONVERT(VARCHAR(25),COMPENTRADA) = @ID_MOVIMENTO
											   AND OBSERVACAODEVOLUCAO IS NULL),'')
								  ELSE ''
							 END
			,HISTORICOLONGO = CASE WHEN  EXISTS (SELECT DadosAdicionais FROM INTEGRACAOTE..FATURA_NOTA_FISCAL WHERE FATURANOTAFISCAL =@ID_MOVIMENTO)
								  THEN ISNULL((SELECT TOP 1 REPLACE(DadosAdicionais,CHAR(13),' - ') FROM INTEGRACAOTE..FATURA_NOTA_FISCAL WHERE FATURANOTAFISCAL =@ID_MOVIMENTO),'')
								  ELSE ''
							  END 
		                      
		WHERE  IDMOV          =  (SELECT  TOP 1 H.IDMOV FROM CORPORERMATT..TMOVHISTORICO H, TMOV M 
								  WHERE   H.IDMOV        = M.IDMOV
								  AND     H.CODCOLIGADA  = M.CODCOLIGADA
								  AND     M.CAMPOLIVRE1  = @ID_MOVIMENTO)
		AND    CODCOLIGADA    = @CODCOLIGADA      
	END /*FIM UPDATE HISTORICO CURTO*/   
                 
            --PRINT 'UPDATE'                

           IF NOT EXISTS (SELECT  H.IDMOV FROM CORPORERMATT..TMOVHISTORICO H, TMOV M 
                          WHERE   H.IDMOV        = M.IDMOV
                          AND     H.CODCOLIGADA  = M.CODCOLIGADA
                          AND     M.CAMPOLIVRE1  = @ID_MOVIMENTO) 
 
            BEGIN
                        INSERT CORPORERMATT..TMOVHISTORICO
                        VALUES (
                                @CODCOLIGADA
                               ,(SELECT  TOP 1 M.IDMOV FROM TMOV M 
                                 WHERE   M.CODCOLIGADA  = @CODCOLIGADA
                                 AND     M.CAMPOLIVRE1  = @ID_MOVIMENTO) 
                     
                               ,(CASE WHEN  EXISTS (SELECT OBSERVACAODEVOLUCAO FROM INTEGRACAOTE..COMP_ENTRADA WHERE CONVERT(VARCHAR(25),COMPENTRADA) =@ID_MOVIMENTO AND OBSERVACAODEVOLUCAO IS NULL)
                                     THEN ISNULL((SELECT TOP 1 OBSERVACAODEVOLUCAO FROM INTEGRACAOTE..COMP_ENTRADA WHERE  CONVERT(VARCHAR(25),COMPENTRADA) =@ID_MOVIMENTO AND OBSERVACAODEVOLUCAO IS NULL),'')
                                     ELSE ''
                                 END)                         
                               ,(CASE WHEN  EXISTS (SELECT DadosAdicionais FROM INTEGRACAOTE..FATURA_NOTA_FISCAL WHERE FATURANOTAFISCAL =@ID_MOVIMENTO)
                                     THEN ISNULL((SELECT TOP 1 REPLACE(DadosAdicionais,CHAR(13),' - ') FROM INTEGRACAOTE..FATURA_NOTA_FISCAL WHERE FATURANOTAFISCAL =@ID_MOVIMENTO),'')
                                     ELSE ''
                                 END)
  , NULL 
                               , NULL
                               , NULL
                               , NULL
                                )                                
            END /*FIM INSERT HISTORICO CURTO*/
            --PRINT 'INSERCAO'
END/*FIM SINCRONISMO DE HISTORICO CURTO*/ 
  


/*          I T E N S          D O          M O V I M E N T O         */ 
------------------------------------------------------------------------  
/*BLOCO PARA INSERÇÃO DOS ITENS E TRIBUTOS DOS ITENS                   */
/*ATENCAO: A PROCEDURE SP_ITENMOVIMENTO É CHAMADA E É ELA QUEM FAZ     */
/*         PESQUISA DOS ITENS DA NF, INSERE E INCLUI TODA A TRIBUTAÇÃO */
/*         RELACIONADA AOS ITENS                                       */
------------------------------------------------------------------------+
DECLARE @IDMOV_E INT;

SET @IDMOV_E = (SELECT IDMOV FROM CORPORERMATT..TMOV WHERE CAMPOLIVRE1 = @ID_MOVIMENTO AND CODCOLIGADA =@CODCOLIGADA) 

IF (Substring(@CAMPOLIVRE1,1,3) = 'CTE') /*ROBERTO*/
BEGIN
	SET @IDMOV_E = (SELECT IDMOV FROM CORPORERMATT..TMOV WHERE CAMPOLIVRE1 = @CAMPOLIVRE1 AND CODCOLIGADA =@CODCOLIGADA)
	SET @ID_MOVIMENTO=@CAMPOLIVRE1
END


EXEC CORPORERMATT..SP_ITEMMOVIMENTO
                      @ID_MOVIMENTO
                      ,@CODCOLIGADA
                      ,@IDMOV_E
                      ,@TIPO_MOV

/*FIM DO BLOCO PARA INSERÇÃO DOS ITENS E TRIBUTOS DOS ITENS     */
  
  
            
END /****** F I M -> V A L I D A Ç Ã O    D E    E X I S T E N C I A   D E    M O V    N A    A Ç O   V I S A *****/
GO

